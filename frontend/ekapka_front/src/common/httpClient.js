// Used for http requests.

import * as apiPaths from './apiPaths';

const axios = require('axios');

// Tu teoretycznie można dać  axios.create({ baseURL: apiPaths.API, withCredentials: true,});
// Ale można zrobić to też w post w formularzu logowania 

export default () => {
  return axios.create({ withCredentials: true, baseURL: apiPaths.API });
};

export function nominatimReverse(lat, lon) { 
  return axios.create({ baseURL: apiPaths.NOMINATIM_API + 'reverse?format=json&lat=' + lat + '&lon=' + lon });
}

export function nominatimSearch(country, state, city, street, building, postcode) {
  const delimiter = '%2C+';
  const url = apiPaths.NOMINATIM_API + 'search.php?q=' + country + delimiter + state + delimiter + city + delimiter + street + delimiter + building /*+ delimiter + postcode*/ + '&format=json' ;
  return axios.create({ baseURL: url });
}