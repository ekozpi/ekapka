// File where paths TO OUR API are being stored.

export const API = 'https://localhost:44357/';//'https://localhost:5001/';
export const NOMINATIM_API = 'https://nominatim.openstreetmap.org/';

// Controllers paths
export const ECOPOINTS = 'ecopoints';
export const ECOPOINTTYPES = 'ecopointtypes';
export const ECOPOINTTYPEDETAIL = 'ecopointtypedetails';
export const AUTH = 'auth';

// Actions paths
export const GET = 'get';
export const GET_ALL = 'getAll';
export const ADD = 'add';
export const EDIT = 'edit';
export const REMOVE = 'remove';
export const REGISTER = 'register';
export const LOGIN = 'login';
export const LOGOUT = 'logout';
