// File where FRONTEND paths are being stored.

export const APP = 'http://localhost:3000';

export const HOME_PAGE = '/';
export const ECO_POINT_DETAILS = '/ecopoints';
export const CATEGORY_DESCRIPTION_PAGE = '/categories';
export const USERS = '/users';
export const REGISTER = '/register';
export const LOGIN = '/login';
