import * as React from 'react';
import { Link } from 'react-router-dom';
import * as paths from '../../common/paths';
import * as CategoryColor from '../../utils/CategoryColor';
import './styles/eco-points-list-element.scss';

type EcoPoint = {
    id: string;
    date: string;
    longitude: string;
    latitude: string;
    name: string;
    typeSlug: string;
    typeId: number;
    address: {
        id: Number,
        country: string,
        state: string,
        city: string,
        street: string,
        building: string,
        flat: string,
        postcode: string
    }
};

interface ISelfProps {
    data: EcoPoint,
}

export default class EcoPointsListElement extends React.Component<ISelfProps> {

    render() {
        return (
            <Link to={paths.ECO_POINT_DETAILS + '/' + this.props.data.id}  >
                <div
                    className="ecoPointsListelement "
                    style={{ borderLeftColor: CategoryColor.getCategoryColorByTypeSlug(this.props.data.typeSlug) }}
                >
                    <div className="titleRow">
                        <div className="description">
                            {this.props.data.name}
                        </div>
                        <div className="date">
                            {this.props.data.date.substring(0, 10)}
                        </div>
                    </div>

                    <div className="address">
                        {'ul. ' + this.props.data.address.street + ', ' + this.props.data.address.building} {this.props.data.address.flat ? ('/' + this.props.data.address.flat) : ('')}
                    </div>

                    <div className="address">
                        {this.props.data.address.postcode + ', ' + this.props.data.address.city}
                        <br />
                    </div>
                </div>
            </Link>
        );
    }
}