import { IconButton, InputAdornment, TextField, ThemeProvider } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import * as React from 'react';
import * as MaterialUITheme from '../../utils/MaterialUITheme';
import EcoPointsListElement from './EcoPointsListElement';
import './styles/eco-points-list.scss';

interface Props {
    points: any[],
    filterMethod: any,
}

export default class EcoPointsList extends React.Component<Props> {

    private renderElements() {
        return (
            this.props.points.map(point => {
                return <EcoPointsListElement data={point} />;
            })
        );
    }

    render() {

        return (
            <div className="scrollListWrapper">
                <div className="filterInput">
                    <ThemeProvider theme={MaterialUITheme.getGreenTheme()} >
                        <TextField
                            placeholder="wyszukaj frazę"
                            variant="outlined"
                            margin="dense"
                            fullWidth
                            InputProps={{
                                endAdornment: (
                                    <InputAdornment position="end">
                                        <IconButton>
                                            <SearchIcon />
                                        </IconButton>
                                    </InputAdornment>
                                ),
                                style: {
                                    backgroundColor: '#ffffff'
                                }
                            }}
                            onChange={(e) => this.props.filterMethod(e.target.value)} />
                    </ThemeProvider>
                </div>
                <div className="ecoPointsList">
                    {this.renderElements()}
                </div>
            </div>
        );
    }

}
