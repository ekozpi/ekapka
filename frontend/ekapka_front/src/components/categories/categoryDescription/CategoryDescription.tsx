import * as React from 'react';
import * as CategoryColor from '../../../utils/CategoryColor';
import CategoryDescriptionHint from './CategoryDescriptionHint';
import './styles/categoryDescription.scss';

type Hint = {
    title: string;
    content: string;
    url: string;
};

type CategoryDetail = {
    name: string;
    description: string;
    slug: string;
    hints: Hint[];
};

interface ISelfProps {
    details: CategoryDetail
}

export default class CategoryDescription extends React.Component<ISelfProps> {

    private createMarkup() {
        return { __html: this.props.details.description };
    }

    private renderHint() {
        return this.props.details.hints.map((hintElem, key) => {
            return <CategoryDescriptionHint hint={hintElem} typeSlug={this.props.details.slug} />;
        });
    }

    render() {
        return (
            <div className="container" style={{ borderColor: CategoryColor.getCategoryColorByTypeSlug(this.props.details.slug) }}>

                <div className="column" >
                    <h3>{this.props.details.name}</h3>
                    <div id="article" dangerouslySetInnerHTML={this.createMarkup()} ></div>
                </div>

                <div className="column" >
                    {this.renderHint()}
                </div>

            </div>
        );
    }

}