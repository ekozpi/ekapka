import * as React from 'react';
import * as CategoryColor from '../../../utils/CategoryColor';
import './styles/categoryDescription.scss';

type Hint = {
    title: string;
    content: string;
    url: string;
};

interface ISelfProps {
    hint: Hint;
    typeSlug: string;
}

export default class CategoryDescriptionHint extends React.Component<ISelfProps> {

    render() {
        return (
            <a
                href={this.props.hint.url} style={{ backgroundColor: CategoryColor.getCategoryLighterColorByTypeSlug(this.props.typeSlug) }}
                className="hintContainer" target="blank"
            >
                <div className="hintTitle">{this.props.hint.title}</div>
                <div>{this.props.hint.content}</div>
            </a>
        );
    }

}