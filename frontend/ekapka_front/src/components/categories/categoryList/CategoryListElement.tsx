import * as React from 'react';
import * as CategoryColor from '../../../utils/CategoryColor';
import './styles/categoryListElement.scss';

type Category = {
    id: number;
    name: string;
    slug: string;
};

interface ISelfProps {
    data: Category,
    onClick: any,
    ifSelected: boolean;
}

export default class categoryListElement extends React.Component<ISelfProps> {

    render() {
        return (
            <div
                className="categoryListElement "
                onClick={() => {
                    this.props.onClick(this.props.data.slug);
                }}
                style={{
                    borderColor: CategoryColor.getCategoryColorByTypeSlug(this.props.data.slug),
                    zIndex: (this.props.ifSelected ? 40 : 0)
                }}
            >
                {this.props.data.name}
            </div>
        );
    }
}