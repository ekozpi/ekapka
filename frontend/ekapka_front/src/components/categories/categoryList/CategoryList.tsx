import * as React from 'react';
import CategoryListElement from './CategoryListElement';
import './styles/categoryList.scss';

type Category = {
    id: number;
    name: string;
    slug: string;
};

interface ISelfProps {
    elements: Category[]
    onClick: any,
    selectedSlug: string,
}

export default class CategoryList extends React.Component<ISelfProps> {

    handleCategorySelection = (categorySlug: string) => {
        this.props.onClick(categorySlug);
    };

    private renderElements() {
        return (
            this.props.elements.map(elem => {
                return <CategoryListElement data={elem} onClick={this.handleCategorySelection} ifSelected={elem.slug === this.props.selectedSlug} />;
            })
        );
    }

    render() {
        return (
            <div className="CategoryList">
                {this.renderElements()}
            </div>
        );
    }

}