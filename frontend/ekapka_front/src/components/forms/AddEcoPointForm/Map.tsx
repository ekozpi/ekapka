import { LatLng } from 'leaflet';
import React from 'react';
import { Map as LeafletMap, Marker, TileLayer } from 'react-leaflet';
import { getDefaultLocation } from '../../../utils/userUtils';
import './styles/map-style.scss';

interface ISelfState {
    zoom: number;
}

interface IProps {
    onMapClick: any;
    markerLat: any;
    markerLon: any;
}

export default class Map extends React.Component<IProps, ISelfState> {

    state = {
        zoom: 16,
    };

    handleMapClick(e: any) {
        const { lat, lng } = e.latlng;
        this.props.onMapClick(lat, lng);
    }

    render() {
        const centerPosition = (this.props.markerLat === '' && this.props.markerLon === '') ? new LatLng(getDefaultLocation().lat, getDefaultLocation().lon) : new LatLng(this.props.markerLat, this.props.markerLon);

        return (
            <LeafletMap style={{ height: '200px' }} onClick={(e: any) => this.handleMapClick(e)} center={centerPosition} zoom={this.state.zoom}>
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

                {(this.props.markerLat !== '' && this.props.markerLon !== '')
                    ?
                    <Marker position={[Number(this.props.markerLat), Number(this.props.markerLon)]} />
                    :
                    <></>
                }

            </LeafletMap>
        );
    }
}
