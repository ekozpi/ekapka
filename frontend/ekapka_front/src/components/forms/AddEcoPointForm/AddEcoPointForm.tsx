import { Button, DialogActions, DialogContent, DialogTitle, MenuItem, Select, TextField } from '@material-ui/core';
import axios from 'axios';
import React from 'react';
import Reaptcha from 'reaptcha';
import * as apiPaths from '../../../common/apiPaths';
import httpClient, { nominatimReverse, nominatimSearch } from '../../../common/httpClient';
import Map from './Map';
import './styles/point-form.scss';

interface IProps {
    onClose: any
}

interface ISelfState {
    categories: any[],
    name: string,
    errorName: boolean,
    description: string,
    categoryId: number,
    errorCategory: boolean,
    lat: string,
    errorLat: boolean,
    lon: string,
    errorLon: boolean,
    country: string,
    state: string,
    city: string,
    street: string,
    building: string,
    postcode: string,
    captchaVerified: boolean
}

// required to abort axios request
const CancelToken = axios.CancelToken;
let cancel: any;

export default class AddEcoPointForm extends React.Component<IProps, ISelfState> {
    state = {
        categories: [],
        name: '',
        errorName: false,
        description: '',
        categoryId: 0,
        errorCategory: false,
        lat: '',
        errorLat: false,
        lon: '',
        errorLon: false,
        country: '',
        state: '',
        city: '',
        street: '',
        building: '',
        postcode: '',
        captchaVerified: false
    };

    componentDidMount() {
        httpClient().get(apiPaths.ECOPOINTTYPES)
            .then((response: any) => {
                this.setState({ categories: response.data });
            })
            .catch((error: any) => {
                console.log(error);
            });
    }

    validate(): Boolean {
        let isValid = true;

        if (this.state.name.trim() === '') {
            isValid = false;
            this.setState({ errorName: true });
        } else {
            this.setState({ errorName: false });
        }

        if (this.state.categoryId === 0) {
            isValid = false;
            this.setState({ errorCategory: true });
        } else {
            this.setState({ errorCategory: false });
        }

        if (this.state.lat.replace('-', '').trim() === '') {
            isValid = false;
            this.setState({ errorLat: true });
        } else {
            this.setState({ errorLat: false });
        }

        if (this.state.lon.replace('-', '').trim() === '') {
            isValid = false;
            this.setState({ errorLon: true });
        } else {
            this.setState({ errorLon: false });
        }

        return isValid;
    }

    handleCoordinatesChanged(e: any, target: string) {
        const value = e.target.value;
        if (value === '' || value.match(/^[+-]?\d+\.?\d*$/)) {
            this.setState({ [target]: value } as Pick<ISelfState, keyof ISelfState>, () => {
                this.updateAddressFromCoords();
            });
        }
    }

    handleAccept() {
        if (this.validate() && this.state.captchaVerified) {
            const newPoint = {
                Id: 0,
                TypeId: this.state.categoryId,
                Longitude: Number(this.state.lon),
                Latitude: Number(this.state.lat),
                Name: this.state.name,
                Description: this.state.description,
                Address: {
                    Id: 0,
                    Country: this.state.country,
                    State: this.state.state,
                    City: this.state.city,
                    Street: this.state.street,
                    Building: this.state.building,
                    Flat: '',
                    Postcode: this.state.postcode
                }
            };

            httpClient().post(apiPaths.ECOPOINTS, newPoint)
                .then((response: any) => {
                    window.location.reload();
                })
                .catch((error: any) => {
                    alert('err' + error + '\n' + error.message);
                    console.log(error);
                });
        }
    }

    updateAddressFromCoords() {
        nominatimReverse(this.state.lat, this.state.lon).get()
            .then((response: any) => {
                this.setState({
                    country: response.data.address.country == null ? '' : response.data.address.country,
                    state: response.data.address.state == null ? '' : response.data.address.state,
                    city: response.data.address.city == null ? '' : response.data.address.city,
                    street: response.data.address.road == null ? '' : response.data.address.road,
                    building: response.data.address.house_number == null ? (response.data.address.building == null ? '' : response.data.address.building) : response.data.address.house_number,
                    postcode: response.data.address.postcode == null ? '' : response.data.address.postcode
                });
            })
            .catch((error: any) => {
                console.log(error);
            });
    }

    handleMapClicked = (lat: number, lon: number) => {
        this.setState({ lat: String(lat), lon: String(lon) }, () => this.updateAddressFromCoords());
    };

    handleAddressChanged(name: any, value: String) {
        if (cancel !== undefined) {
            cancel();
        }

        this.setState({ [name]: value } as Pick<ISelfState, keyof ISelfState>, () => {
            nominatimSearch(this.state.country, this.state.state, this.state.city, this.state.street, this.state.building, this.state.postcode)
                .get('', { cancelToken: new CancelToken(((c) => cancel = c)) })
                .then((response: any) => {
                    this.setState({ lat: response.data[0].lat, lon: response.data[0].lon });
                    console.log(response);
                })
                .catch((error: any) => {
                    console.log(error);
                });
        });
    }

    onCaptchaVerified() {
        this.setState({ captchaVerified: true });
    }

    render() {
        return (
            <div>
                <DialogTitle>Dodaj nowy Eko-Punkt</DialogTitle>
                <DialogContent>
                    <div>
                        <TextField error={this.state.errorName} label="Nazwa" variant="outlined" margin="dense" fullWidth onChange={(e) => this.setState({ name: e.target.value })} />
                        <TextField label="Opis" variant="outlined" margin="dense" fullWidth multiline onChange={(e) => this.setState({ description: e.target.value })} />
                    </div>
                    <div>
                        <Select style={{ marginTop: '6px', marginBottom: '2px' }} fullWidth error={this.state.errorCategory} variant="outlined" margin="dense" onChange={(e) => this.setState({ categoryId: Number(e.target.value) })}>
                            {this.state.categories.map((c: any) => <MenuItem value={c.id}>{c.name}</MenuItem>)}
                        </Select>
                    </div>
                    <div className="twoInputs">
                        <TextField error={this.state.errorLat} value={this.state.lat} label="Szerokość" variant="outlined" margin="dense" onChange={(e) => this.handleCoordinatesChanged(e, 'lat')} />
                        <TextField error={this.state.errorLon} value={this.state.lon} label="Długość" variant="outlined" margin="dense" onChange={(e) => this.handleCoordinatesChanged(e, 'lon')} />
                    </div>
                    <div>
                        <TextField id='country' value={this.state.country} label="Państwo" variant="outlined" margin="dense" fullWidth onChange={(e) => this.handleAddressChanged('country', e.target.value)} />
                        <TextField value={this.state.state} label="Województwo" variant="outlined" margin="dense" fullWidth onChange={(e) => this.handleAddressChanged('state', e.target.value)} />
                        <TextField value={this.state.city} label="Miasto" variant="outlined" margin="dense" fullWidth onChange={(e) => this.handleAddressChanged('city', e.target.value)} />
                        <TextField value={this.state.street} label="Ulica" variant="outlined" margin="dense" fullWidth onChange={(e) => this.handleAddressChanged('street', e.target.value)} />
                    </div>
                    <div className="twoInputs">
                        <TextField value={this.state.building} label="Budynek" variant="outlined" margin="dense" onChange={(e) => this.handleAddressChanged('building', e.target.value)} />
                        <TextField value={this.state.postcode} label="Kod pocztowy" variant="outlined" margin="dense" onChange={(e) => this.handleAddressChanged('postcode', e.target.value)} />
                    </div>

                    <div className="map">
                        <Map onMapClick={this.handleMapClicked} markerLat={this.state.lat} markerLon={this.state.lon} />
                    </div>

                    <div className="captcha">
                        <Reaptcha sitekey="6LcZv_gUAAAAACu3k7AhLO7Bb0azEQmIyr2vzZK_" onVerify={() => this.onCaptchaVerified()} />
                    </div>
                </DialogContent>

                <DialogActions>
                    <Button onClick={() => this.props.onClose()} color="primary">
                        Anuluj
                    </Button>
                    <Button onClick={() => this.handleAccept()} color="primary">
                        Dodaj
                    </Button>
                </DialogActions>
            </div>
        );
    }
}