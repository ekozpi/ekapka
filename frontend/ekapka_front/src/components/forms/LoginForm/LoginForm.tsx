import { Button, Checkbox, TextField, ThemeProvider } from '@material-ui/core';
import * as React from 'react';
import * as apiPaths from '../../../common/apiPaths';
import httpClient from '../../../common/httpClient';
import * as paths from '../../../common/paths';
import * as MaterialUITheme from '../../../utils/MaterialUITheme';
import './styles/login-form-style.scss';

interface ISelfState {
    login: string,
    password: string,
    remember: boolean
}

export default class LoginForm extends React.Component<any, ISelfState> {

    state = {
        login: '',
        password: '',
        remember: false,
    };

    private handleLogin() {
        const loginUser = {
            Login: this.state.login,
            Password: this.state.password,
            RememberMe: this.state.remember
        };

        httpClient().post(apiPaths.AUTH + '/' + apiPaths.LOGIN, loginUser)
            .then((response: any) => {
                window.location.href = paths.APP + paths.HOME_PAGE;
            })
            .catch((error: any) => {
                alert('err' + error + '\n' + error.message);
                console.log(error);
            });
    }

    render() {
        return (
            <div className="form-container">
                <ThemeProvider theme={MaterialUITheme.getGreenTheme()} >
                    <div className="login-form">
                        <div className="title">
                            Zaloguj się
                        </div>

                        <TextField
                            label="login"
                            variant="outlined"
                            margin="dense"
                            onChange={(e) => {
                                this.setState({ login: e.target.value });
                            }}
                            fullWidth
                        />

                        <TextField
                            label="password"
                            type="password"
                            variant="outlined"
                            margin="dense"
                            onChange={(e) => {
                                this.setState({ password: e.target.value });
                            }}
                            fullWidth
                        />

                        <div className="remember-me-line">
                            <Checkbox
                                value="checkedA"
                                onChange={(e) => this.setState({ remember: e.target.checked })}
                                inputProps={{ 'aria-label': 'Checkbox A' }}
                            />
                            Zapamiętaj mnie
                        </div>

                        <Button
                            variant="contained"
                            color="primary"
                            fullWidth
                            onClick={() => this.handleLogin()}>
                            ZALOGUJ SIĘ
                        </Button>
                    </div>

                    <div className="link-to-login">
                        <div className="label"> Nie masz jeszcze konta? <br /> Załóż je i zostań eko! </div>
                        <Button variant="contained" color="secondary" href={paths.APP + paths.USERS + paths.REGISTER}> ZAŁÓŻ KONTO </Button>
                    </div>
                </ThemeProvider>
            </div>
        );
    }
}