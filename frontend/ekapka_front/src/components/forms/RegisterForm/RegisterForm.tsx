import { Button, Checkbox, TextField, ThemeProvider } from '@material-ui/core';
import * as React from 'react';
import * as apiPaths from '../../../common/apiPaths';
import httpClient from '../../../common/httpClient';
import * as paths from '../../../common/paths';
import * as MaterialUITheme from '../../../utils/MaterialUITheme';
import './styles/register-form-style.scss';

interface ISelfState {
    login: string,
    email: string,
    password1: string,
    password2: string,
    loginAuth: number,
    emailAuth: number,
    password1Auth: number,
    password2Auth: number,
    termsAuth: boolean,
}

export default class RegisterForm extends React.Component<any, ISelfState> {

    state = {
        login: '',
        email: '',
        password1: '',
        password2: '',
        loginAuth: 0,
        emailAuth: 0,
        password1Auth: 0,
        password2Auth: 0,
        termsAuth: false,
    };

    private checkUsername(login: string) {

        const allowed = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+';
        const needed1 = 'abcdefghijklmnopqrstuvwxyz';
        const needed2 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let ned1 = false, ned2 = false;

        this.setState({ login: login });

        if (!login) {
            this.setState({ loginAuth: 0 });
            return;
        }

        if (login.length < 5 || login.length > 20) {
            this.setState({ loginAuth: -1 });
            return;
        }

        for (let a = 0; a < login.length; a++) {

            if (needed1.includes(login.charAt(a))) ned1 = true;
            if (needed2.includes(login.charAt(a))) ned2 = true;

            if (!allowed.includes(login.charAt(a))) {
                this.setState({ loginAuth: -1 });
                return;
            }
        }

        if (ned1 && ned2) {
            this.setState({ loginAuth: 1 });
        } else {
            this.setState({ loginAuth: -1 });
        }

    }

    private checkPassword(pass: string) {

        this.setState({ password1: pass });

        if (!pass) {
            this.setState({ password1Auth: 0 });
            return;
        }

        pass = pass.replace(' ', '');

        if (pass.length < 8 || pass.length > 25) {
            this.setState({ password1Auth: -1 });
            return;
        }

        const needed1 = '1234567890';
        const needed2 = '!#^*%$-._@+';

        let ned1 = false;
        let ned2 = false;

        for (let a = 0; a < pass.length; a++) {
            if (needed1.includes(pass.charAt(a))) ned1 = true;
            if (needed2.includes(pass.charAt(a))) ned2 = true;
        }

        if (!ned1 || !ned2)
            this.setState({ password1Auth: -1 });
        else
            this.setState({ password1Auth: 1 });
    }

    private checkSecondPassword(pass: string) {

        this.setState({ password2: pass });

        if (!pass) {
            this.setState({ password2Auth: 0 });
            return;
        }

        if (this.state.password1 === pass) {
            this.setState({ password2Auth: 1 });
        } else {
            this.setState({ password2Auth: -1 });
        }

    }

    private checkEmail(email: string) {

        this.setState({ email: email });

        if (!email) {
            this.setState({ emailAuth: 0 });
            return;
        }

        if (!email.includes('@')) {
            this.setState({ emailAuth: -1 });
            return;
        }

        const postfix = email.substring(email.search('@'));

        if (postfix.includes('.') && postfix.charAt(1) !== '.' && postfix.charAt(postfix.length - 1) !== '.') {
            this.setState({ emailAuth: 1 });
            return;
        } else {
            this.setState({ emailAuth: -1 });
            return;
        }

    }

    private validateAndCreate() {

        if (this.state.loginAuth === 1 && this.state.emailAuth === 1 && this.state.password1Auth === 1 && this.state.password2Auth === 1 && this.state.termsAuth) {

            const newUser = {
                Login: this.state.login,
                Email: this.state.email,
                Password: this.state.password1,
                ConfirmPassword: this.state.password2
            };

            httpClient().post(apiPaths.AUTH + '/' + apiPaths.REGISTER, newUser)
                .then((response: any) => {
                    alert('Użytkownik zarejestrowany pomyślnie!');
                    window.location.href = paths.APP + paths.USERS + paths.LOGIN;
                })
                .catch((error: any) => {
                    alert('err' + error + '\n' + error.message);
                    console.log(error);
                });

        } else {
            alert('Nie można zarejestrować. Dane nie są wypełnione poprawnie!');
        }
    }

    render() {
        return (

            <div className="formContainer">

                <ThemeProvider theme={MaterialUITheme.getGreenTheme()} >

                    <div className="registerForm">

                        <div className="title">
                            Zostań eko, to nic nie kosztuje!
                </div>

                        <TextField
                            label="login"
                            variant="outlined"
                            margin="dense"
                            error={this.state.loginAuth === -1}
                            helperText={this.state.loginAuth === -1 ? 'Login musi zawierać od 5 do 20 znaków' : null}
                            fullWidth onChange={(e) => this.checkUsername(e.target.value)} />

                        <TextField
                            label="e-mail"
                            variant="outlined"
                            margin="dense"
                            error={this.state.emailAuth === -1}
                            helperText={this.state.emailAuth === -1 ? 'Email wydaje się być niepoprawny' : null}
                            fullWidth onChange={(e) => this.checkEmail(e.target.value)} />

                        <TextField
                            label="hasło"
                            variant="outlined"
                            margin="dense"
                            type="password"
                            error={this.state.password1Auth === -1}
                            helperText={this.state.password1Auth === -1 ? 'Musi zawierać od 8 do 20 znaków,przynajmniej jedną małą i dużą literę oraz przynajmniej jedną cyfrę i znak {!,#,^,*,%,$,-,.,_,@,+}' : null}
                            fullWidth onChange={(e) => this.checkPassword(e.target.value)} />

                        <TextField
                            label="powtórz hasło"
                            variant="outlined"
                            margin="dense"
                            type="password"
                            error={this.state.password2Auth === -1}
                            helperText={this.state.password2Auth === -1 ? 'Hasła muszą być takie same' : null}
                            fullWidth onChange={(e) => this.checkSecondPassword(e.target.value)} />

                        <div className="inputLineTerms">
                            <Checkbox
                                value="checkedA"
                                onChange={(e) => this.setState({ termsAuth: e.target.checked })}
                                inputProps={{ 'aria-label': 'Checkbox A' }} />
                  Oświadczam że przeczytałem i akceptuję <a href="https://ekapka.eko/regulamin" >regulamin</a> serwisu.
                </div>

                        <Button
                            variant="contained"
                            color="primary"
                            fullWidth
                            onClick={() => this.validateAndCreate()}>
                            ZOSTAŃ EKO
                </Button>

                    </div>

                    <div className="linkToLogin">
                        <div className="label"> Już jesteś eko? </div>
                        <Button variant="contained" color="secondary" href={paths.APP + paths.USERS + paths.LOGIN}> ZALOGUJ SIĘ </Button>
                    </div>

                </ThemeProvider>
            </div>
        );
    }

}
