/* eslint-disable no-mixed-spaces-and-tabs */
import React from 'react';
import { Map as LeafletMap, Marker, Popup, TileLayer } from 'react-leaflet';
import MarkerClusterGroup from 'react-leaflet-markercluster';
import { Link } from 'react-router-dom';
import * as paths from '../../common/paths';
import * as MapIcon from '../../utils/MapIcon';
import { getDefaultLocation } from '../../utils/userUtils';
import './styles/map-style.scss';

interface Props {
    points: any[],
}

interface ISelfState {
    location: any,
    zoom: number,
}

export default class Map extends React.Component<Props, ISelfState> {
    state = {
        location: getDefaultLocation(),
        zoom: 16,
    };

    private createPopupDescription(point: any) {
        return (
            <div className="popupContainer" >
                <div style={{ fontSize: 16 }}>
                    <b>{point.name}</b>
                </div>
                <div>
                    {'ul. ' + point.address.street + ', ' + point.address.building} {point.address.flat ? ('/' + point.address.flat) : ('')}
                </div>

                <div>
                    {point.address.postcode + ', ' + point.address.city}
                    <br />
                </div>
            </div>
        );
    }

    private renderMarkers() {
        return (
            <MarkerClusterGroup showCoverageOnHover={false} removeOutsideVisibleBounds={true} spiderfyOnMaxZoom={false} disableClusteringAtZoom={19} maxClusterRadius={30} >
                {this.props.points.map((point: any, i) => {
                    return (
                        <Marker position={[point.latitude, point.longitude]} icon={MapIcon.getMarkerIconByTypeSlug(point.typeSlug)}>
                            <Link to={paths.ECO_POINT_DETAILS + '/' + point.id}> <Popup >{this.createPopupDescription(point)}</Popup> </Link>
                        </Marker>
                    );
                })}
            </MarkerClusterGroup>
        );
    }

    render() {
        return (
            <LeafletMap center={[this.state.location.lat, this.state.location.lon]} zoom={this.state.zoom} >
                <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    maxZoom={19}
                />
                {this.renderMarkers()}
            </LeafletMap >
        );
    }
}