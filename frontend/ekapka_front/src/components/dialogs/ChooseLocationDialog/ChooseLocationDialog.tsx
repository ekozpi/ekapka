import { Button, Dialog, DialogActions, DialogContent, DialogTitle, MenuItem, Select } from '@material-ui/core';
import React from 'react';
import { nominatimSearch } from '../../../common/httpClient';
import { getDefaultLocation, saveDefaultLocation } from '../../../utils/userUtils';

interface IProps {
    visibility: boolean,
    onClose: any
}

interface IState {
    selected: number
}

const cities = ['Białystok', 'Bielsko-Biała', 'Bydgoszcz', 'Bytom', 'Chorzów', 'Częstochowa', 'Dąbrowa Górnicza', 'Elbląg', 'Gdańsk', 'Gdynia', 'Gliwice', 'Gorzów Wielkopolski', 'Kalisz', 'Katowice', 'Kielce', 'Koszalin', 'Kraków', 'Lublin', 'Łódź', 'Olsztyn', 'Opole', 'Płock', 'Poznań', 'Radom', 'Ruda Śląska', 'Rybnik', 'Rzeszów', 'Sosnowiec', 'Szczecin', 'Tarnów', 'Toruń', 'Tychy', 'Wałbrzych', 'Warszawa', 'Włocławek', 'Wrocław', 'Zabrze', 'Zielona Góra'];

export default class ChooseLocationDialog extends React.Component<IProps, IState> {
    state = {
        selected: cities.findIndex((value) => value === getDefaultLocation().name)
    };

    handleAcceptClicked() {
        nominatimSearch('', '', cities[this.state.selected], '', '', '').get()
            .then((response: any) => {
                saveDefaultLocation(cities[this.state.selected], response.data[0].lat, response.data[0].lon);
                this.props.onClose();
                console.log(response);
                window.location.reload();
            })
            .catch((error: any) => {
                console.log(error);
            });
    }

    render() {
        return (
            <Dialog open={this.props.visibility} onClose={() => this.props.onClose()}>
                <DialogTitle>Wybierz domyslną lokalizację</DialogTitle>

                <DialogContent>
                    <Select value={this.state.selected} variant="outlined" margin="dense" fullWidth={true} onChange={(e) => this.setState({ selected: (Number)(e.target.value) })}>
                        {cities.map((city, index) => <MenuItem value={index}>{city}</MenuItem>)}
                    </Select>
                </DialogContent>

                <DialogActions>
                    <Button onClick={() => this.props.onClose()} color="primary">
                        Anuluj
                    </Button>

                    <Button disabled={this.state.selected === -1} onClick={() => this.handleAcceptClicked()} color="primary">
                        Ustaw
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }
}
