import { Dialog } from '@material-ui/core';
import React from 'react';
import AddEcoPointForm from '../../forms/AddEcoPointForm/AddEcoPointForm';

interface IProps {
    visibility: boolean,
    onClose: any
}

export default class AddPointDialog extends React.Component<IProps> {
    render() {
        return (
            <Dialog open={this.props.visibility} maxWidth="xs">
                <AddEcoPointForm onClose={this.props.onClose} />
            </Dialog>
        );
    }
}