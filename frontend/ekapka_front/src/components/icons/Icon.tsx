import React from 'react';

interface IconProps {
    width: number,
    height: number,
}

export class Icon extends React.Component<IconProps>{
    static defaultProps = {
        width: 40,
        height: 40,
    };
}