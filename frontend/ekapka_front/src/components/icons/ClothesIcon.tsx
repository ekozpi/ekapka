import React from 'react';
import { ReactComponent as Clothes } from '../../assets/clothes.svg';
import { Icon } from './Icon';

export class ClothesIcon extends Icon {
    render() {
        return (
            <Clothes width={this.props.width} height={this.props.height} />
        );
    }
}