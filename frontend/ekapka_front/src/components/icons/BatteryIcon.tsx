import React from 'react';
import { ReactComponent as Battery } from '../../assets/battery.svg';
import { Icon } from './Icon';

export class BatteryIcon extends Icon {
    render() {
        return (
            <Battery width={this.props.width} height={this.props.height} />
        );
    }
}