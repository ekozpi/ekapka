import React from 'react';
import { ReactComponent as Avatar } from '../../assets/avatar.svg';
import { Icon } from './Icon';

export class AvatarIcon extends Icon {
    render() {
        return (
            <Avatar width={this.props.width} height={this.props.height} />
        );
    }
}