import React from 'react';
import { ReactComponent as Trash } from '../../assets/trash.svg';
import { Icon } from './Icon';

export class TrashIcon extends Icon {
    render() {
        return (
            <Trash width={this.props.width} height={this.props.height} />
        );
    }
}