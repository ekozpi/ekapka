import React from 'react';
import { ReactComponent as Shop } from '../../assets/shop.svg';
import { Icon } from './Icon';

export class ShopIcon extends Icon {
    render() {
        return (
            <Shop width={this.props.width} height={this.props.height} />
        );
    }
}