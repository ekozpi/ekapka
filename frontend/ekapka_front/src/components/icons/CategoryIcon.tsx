import React from 'react';
import { BatteryIcon } from './BatteryIcon';
import { ClothesIcon } from './ClothesIcon';
import { DrugsIcon } from './DrugsIcon';
import { FoodsharingIcon } from './FoodsharingIcon';

interface IconProps {
    categorySlug: string,
    width: number,
    height: number,
}

export default class CategoryIcon extends React.Component<IconProps> {
    static defaultProps = {
        categorySlug: '',
        width: 40,
        height: 40,
    };

    render() {
        return (
            <>
                {this.getCategoryIconById()}
            </>
        );
    }

    private getCategoryIconById() {
        switch (this.props.categorySlug) {
            case 'spent-batteries':
                return <BatteryIcon width={this.props.width} height={this.props.height} />;
            case 'unnecessary-clothes':
                return <ClothesIcon width={this.props.width} height={this.props.height} />;
            case 'overdue-drugs':
                return <DrugsIcon width={this.props.width} height={this.props.height} />;
            case 'foodsharing':
                return <FoodsharingIcon width={this.props.width} height={this.props.height} />;
        }
    }
}