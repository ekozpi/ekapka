import React from 'react';
import { ReactComponent as Foodsharing } from '../../assets/foodsharing.svg';
import { Icon } from './Icon';

export class FoodsharingIcon extends Icon {
    render() {
        return (
            <Foodsharing width={this.props.width} height={this.props.height} />
        );
    }
}