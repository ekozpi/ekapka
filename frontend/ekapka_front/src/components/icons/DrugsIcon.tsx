import React from 'react';
import { ReactComponent as Drugs } from '../../assets/drugs.svg';
import { Icon } from './Icon';

export class DrugsIcon extends Icon {
    render() {
        return (
            <Drugs width={this.props.width} height={this.props.height} />
        );
    }
}