import React from 'react';
import { ReactComponent as Ekapka } from '../../assets/ekapka.svg';
import { Icon } from './Icon';

export class EkapkaIcon extends Icon {
    render() {
        return (
            <Ekapka width={this.props.width} height={this.props.height} />
        );
    }
}