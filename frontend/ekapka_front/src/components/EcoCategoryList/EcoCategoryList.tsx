import * as React from 'react';
import * as apiPaths from '../../common/apiPaths';
import httpClient from '../../common/httpClient';
import EcoCategoryListElement from './EcoCategoryListElement';
import './styles/eco-category-list-style.scss';

interface ListProps {
    onClick: any;
}

interface State {
    elements: any[],
    clicked: string
}

export default class EcoCategoryList extends React.Component<ListProps, State> {
    state = {
        elements: [],
        clicked: ''
    };

    componentDidMount() {
        httpClient().get(apiPaths.ECOPOINTTYPES)
            .then((response: any) => {
                this.setState({ elements: response.data.map((x: any) => x.slug) });
            })
            .catch((error: any) => {
                console.log(error);
            });
    }

    handleItemClicked = (slug: string) => {
        this.props.onClick(slug);
        this.setState({ clicked: (this.state.clicked === slug ? '' : slug) });
    };

    private renderListElements() {
        // fetch data to list elements

        return (
            this.state.elements.map(type => {
                const isClicked = (this.state.clicked === '' ? true : (type === this.state.clicked));
                return <EcoCategoryListElement categorySlug={type} onClick={this.handleItemClicked} clicked={isClicked} />;
            })
        );
    }

    render() {
        return (
            <div className="ecoCategoryList">
                {this.renderListElements()}
            </div>
        );
    }
}