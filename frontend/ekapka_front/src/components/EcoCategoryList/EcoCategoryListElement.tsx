import * as React from 'react';
import CategoryIcon from '../icons/CategoryIcon';
import './styles/eco-category-list-element-style.scss';

interface ElementProps {
    categorySlug: string;
    onClick: any;
    clicked: boolean;
}

export default class EcoCategoryListElement extends React.Component<ElementProps> {
    render() {
        return (
            <div
                className="ecoCategoryListElement"
                onClick={() => this.props.onClick(this.props.categorySlug)}
                style={{ opacity: this.props.clicked === true ? 1 : 0.25 }}>
                <CategoryIcon categorySlug={this.props.categorySlug} />
            </div>
        );
    }
}