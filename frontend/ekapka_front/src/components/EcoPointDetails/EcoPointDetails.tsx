import { Button } from '@material-ui/core';
import { ArrowBackIosSharp } from '@material-ui/icons';
import * as React from 'react';
import { Link, Route } from 'react-router-dom';
import * as apiPaths from '../../common/apiPaths';
import httpClient from '../../common/httpClient';
import * as paths from '../../common/paths';
import NearPointDetails from './NearPointDetails';
import './styles/eco-point-details-style.scss';

type EcoPoint = {
    id: string;
    typeId: string;
    date: string;
    longitude: string;
    latitude: string;
    name: string;
    typeSlug: string;
    description: string;
    address: {
        id: Number,
        country: string,
        state: string,
        city: string,
        street: string,
        building: string,
        flat: string,
        postcode: string
    }
};

type NearPoint = {
    id: string;
    distance: string;
    typeId: string;
    name: string;
};

interface ISelfState {
    closestPoints: NearPoint[],
    data: EcoPoint,
    categoryName: String,
}

interface ISelfProps {
    pointId: string,
}

export default class EcoPointsDetails extends React.Component<ISelfProps,ISelfState> {
 
  state={
    closestPoints: [],
    data: { id:'',
            typeId:'',
            date:'----------', 
            longitude:'',
            latitude:'',
            typeSlug: '',
            typeName: '',
            name:'',
            description:'',
            address: {
              id: 0,
              country: '',
              state: '',
              city: '',
              street: '',
              building: '',
              flat: '',
              postcode: ''
            } },
    categoryName: '',
  };

  functionalSetState = (state:ISelfState) => {
    return state;
  }
  
  componentDidMount(){
    
      httpClient().get(apiPaths.ECOPOINTS + '/closest?id=' + this.getId()+'&closestNumber=3')
        .then((response: any) => {
            this.setState({ closestPoints: response.data });
        })
        .catch((error: any) => {
            console.log(error);
        });

      httpClient().get(apiPaths.ECOPOINTS+'/'+this.getId())
        .then((response: any) => {
          this.setState({ data: response.data });
        })
        .catch((error: any) => {
          console.log(error);
        });
        
  }
 
  componentDidUpdate(){

    if( String(this.state.data.id) !== String(this.props.pointId) ){

        httpClient().get(apiPaths.ECOPOINTS + '/closest?id=' + this.getId()+'&closestNumber=3')
        .then((response: any) => {
          this.setState({ closestPoints: response.data });
        })
        .catch((error: any) => {
          console.log(error);
      });

      httpClient().get(apiPaths.ECOPOINTS+'/'+this.getId())
        .then((response: any) => {
          this.setState({ data: response.data });
      })
      .catch((error: any) => {
        console.log(error);
      });
    }
  }

  renderClosest(){
    return ( 
      this.state.closestPoints.map((element,i) => {
        return(
          <NearPointDetails key={i} point={element} />
        );
      })
    );
  }

  private getId() {
    return this.props.pointId;
  }

  render(){
    return (
      <div className="scrollListWrapper">
        <Route render={({ history })=>(
            <div className="detailsContainer" >

              <div className="navigation">  
                <Button startIcon={<ArrowBackIosSharp />} onClick={()=>history.push(paths.HOME_PAGE)} /> 
                <Link className="categoryLabel" to={ paths.CATEGORY_DESCRIPTION_PAGE+"/"+this.state.data.typeSlug}>{this.state.data.typeName}</Link>
               </div>

              <div className="info-container">

                <div className="title">
                 { 
                    this.state.data.name 
                  }
                </div>

                <div className="details">
                {this.state.data.date.substring(0, 10)}
                </div>

                <div className="details">
                {'ul. ' + this.state.data.address.street + ', ' + this.state.data.address.building} {this.state.data.address.flat ? ('/' + this.state.data.address.flat) : ('')}
                </div>

                <div className="details">
                {this.state.data.address.postcode + ', ' + this.state.data.address.city}
                <br/>
                </div>

                <div className="desc">
                  <br/>
                    {
                      this.state.data.description
                    }
                  </div>

                </div>
          
                <div className="see-also">
                <h3>Zobacz też:  </h3>      
                    {
                      this.renderClosest()
                    }
                </div>
              
            </div>
          
          )} />
    </div>
    );
  }

}