import * as React from 'react';
import './styles/eco-point-details-style.scss';
import { Route } from 'react-router';
import * as paths from '../../common/paths';
import * as CategoryColor from '../../utils/CategoryColor';
import './styles/eco-point-details-style.scss';

type NearPoint = {
	id: string,
	distance: string,
	typeId: string,
	typeSlug: string,
	name: string
};

interface ISelfProps {
    point: NearPoint,
}

export default class NearPointDetails extends React.Component<ISelfProps> {
	render(){
		const distanceFloat = parseFloat(this.props.point.distance);
		
		return(
			<Route render={({ history }) => (
				<div className="close-point" style={{ borderLeftColor: CategoryColor.getCategoryColorByTypeSlug(this.props.point.typeSlug) }} onClick={ () => history.push(paths.ECO_POINT_DETAILS + '/' + this.props.point.id) }  >
					<div className="near-point-label">{ this.props.point.name }</div>
					<div className="near-point-label">Odległość: { distanceFloat > 1 ? distanceFloat.toFixed(2) +' km' : Math.round(distanceFloat*1000) + ' m' }</div>
				</div>
			)} />
		);
	}
}