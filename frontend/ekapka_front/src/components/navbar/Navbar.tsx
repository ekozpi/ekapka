import { Button, Dialog, List, ListItem, Popover } from '@material-ui/core';
import { AccountBox, ExpandMore } from '@material-ui/icons';
import * as React from 'react';
import { Route } from 'react-router';
import * as apiPaths from '../../common/apiPaths';
import httpClient from '../../common/httpClient';
import * as paths from '../../common/paths';
import { isUserLoggedIn } from '../../utils/userUtils';
import ChooseLocationDialog from '../dialogs/ChooseLocationDialog/ChooseLocationDialog';
import AddEcoPointForm from '../forms/AddEcoPointForm/AddEcoPointForm';
import { AvatarIcon } from '../icons/AvatarIcon';
import { EkapkaIcon } from '../icons/EkapkaIcon';
import './styles/navbar-style.scss';

interface IState {
    isAddPointDialogOpen: boolean;
    isChooseLocationDialogOpen: boolean;
    isLoggedIn: boolean;
    popoverAnchorEl: any;
}

export default class Navbar extends React.Component<any, IState> {
    state = {
        isAddPointDialogOpen: false,
        isChooseLocationDialogOpen: false,
        isLoggedIn: isUserLoggedIn(),
        popoverAnchorEl: null
    };

    handleAddPointDialogVisibility = () => {
        this.setState({ isAddPointDialogOpen: !this.state.isAddPointDialogOpen });
    };

    handleChooseLocationDialogVisibility = () => {
        this.setState({ isChooseLocationDialogOpen: !this.state.isChooseLocationDialogOpen });
    };

    handlePopoverOpen = (event: React.MouseEvent<HTMLElement>) => {
        this.setState({ popoverAnchorEl: event.target });
    };

    handlePopoverClose = () => {
        this.setState({ popoverAnchorEl: null });
    };

    handleLogOut = (history: any) => {
        httpClient()
            .post(apiPaths.AUTH + '/' + apiPaths.LOGOUT)
            .then((response: any) => {
                console.log(history);
                window.location.href = paths.APP + paths.HOME_PAGE;
            })
            .catch((error: any) => {
                alert('err' + error + '\n' + error.message);
                console.log(error);
            });
    };

    render() {
        return (
            <div className="navbar">
                <div className="leftSection">
                    {this.renderAppLogo()}
                    {this.renderMenu()}
                </div>
                <div className="rightSection">
                    {this.renderUserActions()}
                </div>
            </div>
        );
    }

    renderAppLogo() {
        return (
            <Route render={({ history }) => (
                <div className="appLogo" onClick={() => history.push(paths.HOME_PAGE)}>
                    <EkapkaIcon height={36} width={36} />
                    <div className="appName">Ekapka</div>
                </div>
            )} />
        );
    }

    renderMenu() {
        return (
            <a className="navbarMenuItem" href={paths.APP + paths.CATEGORY_DESCRIPTION_PAGE}>
                Kategorie
            </a>
        );
    }

    renderUserActions() {
        const accountBoxStyle = { fontSize: '30px', color: '#fff', marginRight: '-10px' };
        const expandMoreStyle = { fontSize: '30px', color: '#fff' };

        return (
            this.state.isLoggedIn
                ?
                <>
                    <div className="menuIcon" onClick={event => this.handlePopoverOpen(event)}>
                        <AvatarIcon height={46} width={46} />
                    </div>

                    <Popover open={Boolean(this.state.popoverAnchorEl)} onClose={this.handlePopoverClose} anchorEl={this.state.popoverAnchorEl}
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                    >
                        <Route render={({ history }) => (
                            <List>
                                <ListItem button onClick={() => {
                                    this.handlePopoverClose();
                                    this.handleAddPointDialogVisibility();
                                }} >
                                    Dodaj punkt
                                </ListItem>
                                <ListItem button onClick={() => {
                                    this.handlePopoverClose();
                                    this.handleChooseLocationDialogVisibility();
                                }} >
                                    Ustaw domyślną lokalizację
                                </ListItem>
                                <ListItem button onClick={() => {
                                    this.handlePopoverClose();
                                    this.handleLogOut(history);
                                }} >
                                    Wyloguj się
                                </ListItem>
                            </List>
                        )} />
                    </Popover>

                    <Dialog open={this.state.isAddPointDialogOpen} maxWidth="xs">
                        <AddEcoPointForm onClose={this.handleAddPointDialogVisibility} />
                    </Dialog>

                    <ChooseLocationDialog visibility={this.state.isChooseLocationDialogOpen} onClose={this.handleChooseLocationDialogVisibility} />
                </>
                :
                <>
                    <Button startIcon={<AccountBox style={accountBoxStyle} />} endIcon={<ExpandMore style={expandMoreStyle} />} onClick={this.handlePopoverOpen} />
                    <Popover open={Boolean(this.state.popoverAnchorEl)} onClose={this.handlePopoverClose} anchorEl={this.state.popoverAnchorEl}
                        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }} transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                    >
                        <Route render={({ history }) => (
                            <List>
                                <ListItem button onClick={() => {
                                    this.handlePopoverClose();
                                    history.push(paths.USERS + paths.LOGIN);
                                }} >
                                    Zaloguj się
                                </ListItem>
                                <ListItem button onClick={() => {
                                    this.handlePopoverClose();
                                    history.push(paths.USERS + paths.REGISTER);
                                }} >
                                    Zarejstruj się
                                </ListItem>
                            </List>
                        )} />
                    </Popover>
                </>
        );
    }
}