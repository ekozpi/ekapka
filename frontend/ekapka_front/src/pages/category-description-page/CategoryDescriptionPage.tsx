import * as React from 'react';
import './styles/category-description-page.scss';
import CategoryDescription from '../../components/categories/categoryDescription/CategoryDescription';
import CategoryList from '../../components/categories/categoryList/CategoryList';
import httpClient from '../../common/httpClient';
import * as apiPaths from '../../common/apiPaths';
import { Url } from 'url';
import * as paths from '../../common/paths';

type Hint = {
    title: string;
    content: string;
    url: Url;
}

type CategoryDetail = {
    name: string;
    slug: string;
    description: string;
    hints:  Hint[];
}

type Category = {
    id: number;
    name: string;
    slug: string;
}

interface ISelfState {
    selectedCategorySlug: string;
    elements: Category[],
    descriptions: CategoryDetail[],
  }

export default class CategoryDescriptionPage extends React.Component<any, ISelfState> {

    state = {
        selectedCategorySlug: "",
        elements: Array<Category>(),
        descriptions: [],
    }
    
    handleCategorySelection = (categorySlug: string) => {
       // this.setState({selectedCategorySlug: categorySlug});
        window.location.pathname = paths.CATEGORY_DESCRIPTION_PAGE+"/"+categorySlug
    }

    getCategorySlugFromUrl = (list: Category[]) =>{
        const url = window.location.pathname;
        const slug = url.substring(url.lastIndexOf('/') + 1);

        const cat = list.filter( (Element:Category)=> Element.slug === slug );

        if(cat.length > 0) 
            return cat[0].slug;
        else
            return list[0].slug;

    }



    componentDidMount() {

        httpClient().get(apiPaths.ECOPOINTTYPES)
          .then((response: any) => {
              this.setState({elements: response.data});
              if(this.state.elements.length > 0 && this.state.selectedCategorySlug === "") this.setState({ selectedCategorySlug: this.getCategorySlugFromUrl(this.state.elements) });
          })
          .then()
          .catch((error: any) => {
              console.log(error)
          });
        console.log("POBIERAM!")
          httpClient().get(apiPaths.ECOPOINTTYPEDETAIL)
          .then((response: any) => {
              this.setState({ descriptions: response.data})
          })
          .catch((error: any) => {
              console.log(error)
          });

    }

    private getDetailsBySlug(){ 
  
        var category = this.state.descriptions.find( (elem: CategoryDetail) => elem.slug === this.state.selectedCategorySlug );
        if(!category){
            return {
                name: " ",
                slug: "",
                description: "Nie dodano jeszcze opisu tej kategori.",
                hints: [],
            };
        }else{
            return category;
        }
     
    }
    

    render() {
        return (
            <div className="mainContainer">
                <div className="categoryList">
                    <CategoryList elements={this.state.elements} onClick={this.handleCategorySelection} selectedSlug={this.state.selectedCategorySlug} />
                </div>
                <div className="categoryDescription" >
                    <CategoryDescription details={ this.getDetailsBySlug() } />
                </div>
            </div>
        )
    }
};