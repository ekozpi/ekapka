In this folder components and styles for pages, are being stored.
For example:
    main-page(folder)
        - styles(folder)
            - main-page.css
            - main-page-map.css
        - MainPage.tsx
        - MainPageMap.tsx