import * as React from 'react';
import './styles/home-page.scss';
import Map from '../../components/map/Map';
import EcoPointsList from '../../components/EcoPointList/EcoPointList';
import EcoCategoryList from '../../components/EcoCategoryList/EcoCategoryList';
import httpClient from '../../common/httpClient';
import * as apiPaths from '../../common/apiPaths';
import { Switch as NestSwitch, Route } from 'react-router-dom';
import EcoPointDetails from '../../components/EcoPointDetails/EcoPointDetails';

interface ISelfState {
  points: any[];
  categoryToShow: string;
  pointsToShow: any[];
  filter: string;
}

export default class HomePage extends React.Component<any, ISelfState> {
  
  state = {
    points: [],
    categoryToShow: '',
    pointsToShow: [],
    filter: '',
  };

  private getId() {
    const url = window.location.pathname;
    return url.substring(url.lastIndexOf('/') + 1);
  }

  componentDidMount() {
    httpClient().get(apiPaths.ECOPOINTS)
      .then((response: any) => {

          this.setState({ points: response.data });
          this.setState({ pointsToShow: this.state.points });
      })
      .catch((error: any) => {
          console.log(error);
      });
  }

  setStateFunctional(newState:ISelfState){
    return newState;
  }

  handleFiltrSelect = (slug:string, input:string) => {

    if(slug === ""){
      
      if(input === ""){
        this.setState(this.setStateFunctional({categoryToShow:"",filter:"", points: this.state.points, pointsToShow: this.state.points}));
      }else{
        const filteredPoints = this.state.points.filter((point:any) => this.isPointFiltred(point,input))
        this.setState(this.setStateFunctional({categoryToShow:"",filter:input,points: this.state.points,pointsToShow: filteredPoints }));
      }

    }else{

      if(input === ""){
        const filteredPoints = this.state.points.filter((point: any) => point.typeSlug === slug);
        this.setState(this.setStateFunctional({categoryToShow:slug,filter:"",points: this.state.points,pointsToShow: filteredPoints }));
      }else{
        const filteredPoints = this.state.points.filter((point: any) => point.typeSlug === slug && this.isPointFiltred(point,input));
        this.setState(this.setStateFunctional({categoryToShow:slug,filter:input,points: this.state.points,pointsToShow: filteredPoints }));
      }

    }

  }

  handleCategorySelection = (slug: string) => {
    if(this.state.categoryToShow === slug)
      this.handleFiltrSelect("", this.state.filter);
    else
     this.handleFiltrSelect(slug, this.state.filter);
  };

  handleFilter = (entry: string) => {
    entry = entry.trim()
    this.handleFiltrSelect(this.state.categoryToShow, entry);
  };

  isPointFiltred = (point:any, entry:string)=>{

      if(point.name.toUpperCase().indexOf(entry.toUpperCase()) !== -1) return true;
      if(point.address.city.toUpperCase().indexOf(entry.toUpperCase()) !== -1) return true;
      if(point.address.street.toUpperCase().indexOf(entry.toUpperCase()) !== -1) return true;
   
    return false;

  };

  render() {
    return (
        <div className="mainPage">
            <div className="mainPageCenterContainer">
            
            <NestSwitch>
              <Route exact path="/"><EcoPointsList points={this.state.pointsToShow} filterMethod={this.handleFilter}/></Route>
              <Route path="/ecopoints"><EcoPointDetails pointId={this.getId()} /></Route>
            </NestSwitch>

                <div className="mapContainer">
                    <Map points={this.state.pointsToShow}  />
                </div>
                
                <EcoCategoryList onClick={this.handleCategorySelection}/>
            </div>
        </div>
    );
  }
}