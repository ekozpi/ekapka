import * as React from 'react';
import "./styles/login-page-style.scss";
import SideImage from '../../../assets/register1.jpg'
import LoginForm from '../../../components/forms/LoginForm/LoginForm';



export default class LoginPage extends React.Component {

  render() {
    return (
        <div className="login-container">

          <LoginForm />

          <div className="photo-column">
            <img src={SideImage} alt="Ekapka_image" />
          </div>
          
        </div>
    );
  }
}