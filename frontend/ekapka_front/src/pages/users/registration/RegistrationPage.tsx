import * as React from 'react';
import "./styles/registration-page-style.scss";
import SideImage from '../../../assets/register1.jpg'
import RegisterForm from '../../../components/forms/RegisterForm/RegisterForm';



export default class RegistrationPage extends React.Component {

  render() {
    return (
        <div className="registrationContainer">

          <RegisterForm />

          <div className="photoColumn">
            <img src={SideImage} alt="Ekapka_image" />
          </div>
          
        </div>
    );
  }
}