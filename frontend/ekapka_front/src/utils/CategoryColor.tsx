
export function getCategoryColorByTypeSlug(typeSlug: string) {
    switch (typeSlug) {
        case 'spent-batteries':
            return ('#1064c4');
        case 'unnecessary-clothes':
            return ('#03704c');
        case 'overdue-drugs':
            return ('#392750');
        case 'foodsharing':
            return ('#ffc114');
        default:
            return ('#333333');
    }
}

export function getCategoryDarkerColorByTypeSlug(typeSlug: string) {
    switch (typeSlug) {
        case 'spent-batteries':
            return ('#2175d5');
        case 'unnecessary-clothes':
            return ('#014d33');
        case 'overdue-drugs':
            return ('#1d0f30');
        case 'foodsharing':
            return ('#ffa600');
        default:
            return ('#000000');
    }
}

export function getCategoryLighterColorByTypeSlug(typeSlug: string) {
    switch (typeSlug) {
        case 'spent-batteries':
            return ('#aecdf5');
        case 'unnecessary-clothes':
            return ('#80bfaa');
        case 'overdue-drugs':
            return ('#8668ad');
        case 'foodsharing':
            return ('#ffdb75');
        default:
            return ('#555555');
    }
}