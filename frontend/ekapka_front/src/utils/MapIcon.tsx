import L from 'leaflet';

const default_size_x = 30;
const default_size_y = 30;

export const getBatteryMarker = (x: number = default_size_x, y: number = default_size_y) =>
    L.icon({ iconUrl: require('../assets/battery.svg'), iconSize: [x, y] });

export const getClothesMarker = (x: number = default_size_x, y: number = default_size_y) =>
    L.icon({ iconUrl: require('../assets/clothes.svg'), iconSize: [x, y] });

export const getDrugsMarker = (x: number = default_size_x, y: number = default_size_y) =>
    L.icon({ iconUrl: require('../assets/drugs.svg'), iconSize: [x, y] });

export const getFoodsharingMarker = (x: number = default_size_x, y: number = default_size_y) =>
    L.icon({ iconUrl: require('../assets/foodsharing.svg'), iconSize: [x, y] });

export function getMarkerIconByTypeSlug(typeSlug: string, x: number = default_size_x, y: number = default_size_y) {
    switch (typeSlug) {
        case 'spent-batteries':
            return getBatteryMarker(x, y);
        case 'unnecessary-clothes':
            return getClothesMarker(x, y);
        case 'overdue-drugs':
            return getDrugsMarker(x, y);
        case 'foodsharing':
            return getFoodsharingMarker(x, y);
    }
}