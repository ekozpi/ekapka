import Cookies from 'js-cookie';

export function isUserLoggedIn(): boolean {
    return Cookies.get('.AspNetCore.Identity.Application') !== undefined;
}

export function saveDefaultLocation(name: string, lat: string, lon: string) {
    Cookies.set('DefaultLocation', { name, lat, lon }, { expires: 365 });
}

export function getDefaultLocation() {
    const location = Cookies.getJSON('DefaultLocation');
    // if not exists -> poland geo center
    return location !== undefined ? location : { name: '', lat: 52.114339, lon: 19.423672 };
}