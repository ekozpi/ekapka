import { createMuiTheme } from '@material-ui/core';

export function getGreenTheme() {
    return createMuiTheme({
        palette: {
            primary: {
                main: '#1d4407',
            },
            secondary: {
                main: '#3e6628'
            },
        }
    });
}

