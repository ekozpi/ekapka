import * as React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './app.scss';
import * as paths from './common/paths';
import Navbar from './components/navbar/Navbar';
import CategoryDescriptionPage from './pages/category-description-page/CategoryDescriptionPage';
import HomePage from './pages/home-page/HomePage';
import LoginPage from './pages/users/login/LoginPage';
import RegistrationPage from './pages/users/registration/RegistrationPage';

export default function App() {
    return (
        <BrowserRouter>
            <Navbar />
            <div className="contentWrapper">
                <Switch>
                    
                    <Route path={paths.CATEGORY_DESCRIPTION_PAGE} component={CategoryDescriptionPage} />
                    <Route path={paths.USERS + paths.REGISTER} component={RegistrationPage}/>
                    <Route path={paths.USERS + paths.LOGIN} component={LoginPage}/>
                    <Route path={paths.HOME_PAGE} component={HomePage} />
                </Switch>
            </div>
        </BrowserRouter>
    );
}
