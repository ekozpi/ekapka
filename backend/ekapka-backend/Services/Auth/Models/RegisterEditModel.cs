﻿namespace EkapkaBackend.Services.Auth
{
    public class RegisterEditModel
    {
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}