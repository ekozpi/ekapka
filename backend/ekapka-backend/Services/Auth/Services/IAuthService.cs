﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace EkapkaBackend.Services.Auth.Services
{
    public interface IAuthService
    {
        public IdentityResult Register(RegisterEditModel model, out IdentityUser user);
        public SignInResult LogIn(LoginEditModel model);
        public Task GenerateConfirmationLinkAndSendEmail(IdentityUser user, Microsoft.AspNetCore.Mvc.IUrlHelper urlHelper, HttpRequest request);
        public Task<IdentityResult> ConfirmEmail(string userId, string token);
        public Task LogOut();
    }
}
