﻿using EkapkaBackend.Controllers;
using EkapkaBackend.Model;
using EkapkaBackend.Services.Auth.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using SignInResult = Microsoft.AspNetCore.Identity.SignInResult;


namespace EkapkaBackend.Services.Auth
{
    public class AuthService : IAuthService
    {
        public readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public AuthService(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        private bool CheckPasswordConfirmation(RegisterEditModel model)
        {
            return model.Password.Equals(model.ConfirmPassword);
        }

        public IdentityResult Register(RegisterEditModel model, out IdentityUser user)
        {
            if (!CheckPasswordConfirmation(model))
            {
                user = null;
                return IdentityResult.Failed();
            }

            user = new IdentityUser { UserName = model.Login, Email = model.Email };
            return _userManager.CreateAsync(user, model.Password).Result;
        }

        public SignInResult LogIn(LoginEditModel model)
        {
            return _signInManager.PasswordSignInAsync(model.Login, model.Password, model.RememberMe, lockoutOnFailure: false).Result;
        }

        public Task LogOut()
        {
            return _signInManager.SignOutAsync();
        }

        public async Task<IdentityResult> ConfirmEmail(string userId, string token)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
                return IdentityResult.Failed();

            return _userManager.ConfirmEmailAsync(user, token).Result;
        }

        public async Task GenerateConfirmationLinkAndSendEmail(IdentityUser user, Microsoft.AspNetCore.Mvc.IUrlHelper urlHelper, HttpRequest request)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            var confirmationLink = urlHelper.Action(nameof(AuthController.VerifyEmail), "Auth", new { userId = user.Id, token }, request.Scheme, request.Host.ToString());
            EmailSender.SendConfirmationEmail(user.Email, user.UserName, confirmationLink);
        }
    }
}
