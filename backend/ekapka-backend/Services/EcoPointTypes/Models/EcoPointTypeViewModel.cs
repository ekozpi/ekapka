﻿using EkapkaBackend.Services.Base.Models;

namespace EkapkaBackend.Services.EcoPointTypes.Models
{
    public class EcoPointTypeViewModel : IViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
    }
}
