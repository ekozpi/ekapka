﻿namespace EkapkaBackend.Services.EcoPointTypes.Models
{
    public class EcoPointTypeHintModel
    {
        public EcoPointTypeHintModel(int id, string title, string content, string url)
        {
            Id = id;
            Title = title;
            Content = content;
            Url = url;
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string Url { get; set; }
    }
}
