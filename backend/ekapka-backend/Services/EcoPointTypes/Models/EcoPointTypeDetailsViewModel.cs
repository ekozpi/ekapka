﻿using EkapkaBackend.Services.Base.Models;
using System.Collections.Generic;

namespace EkapkaBackend.Services.EcoPointTypes.Models
{
    public class EcoPointTypeDetailsViewModel : IViewModel
    {
        public EcoPointTypeDetailsViewModel()
        {
            Hints = new List<EcoPointTypeHintModel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Description { get; set; }
        public List<EcoPointTypeHintModel> Hints { get; set; }
    }
}
