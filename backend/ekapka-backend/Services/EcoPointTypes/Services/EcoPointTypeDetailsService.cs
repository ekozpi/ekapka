﻿using EkapkaBackend.Model;
using EkapkaBackend.Model.Entities;
using EkapkaBackend.Services.Base;
using EkapkaBackend.Services.EcoPointTypes.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EkapkaBackend.Services.EcoPointTypes.Services
{
    public class EcoPointTypeDetailsService : CrudServiceBase<EcoPointType, EcoPointTypeDetailsViewModel>, IEcoPointTypeDetailsService
    {
        public EcoPointTypeDetailsService(EkapkaDbContext dbContext) : base(dbContext)
        {
            dbContext.EcoPointTypes
                .Include(t => t.Details)
                .ThenInclude(d => d.Hints)
                .ToList();
                
        }

        protected override void FillViewModel(EcoPointType entity, EcoPointTypeDetailsViewModel viewModel)
        {
            viewModel.Id = entity.Id;
            viewModel.Name = entity.Name;
            viewModel.Slug = entity.Slug;
            viewModel.Description = entity.Details?.Description;

            FillHints(entity, viewModel);
        }

        private void FillHints(EcoPointType entity, EcoPointTypeDetailsViewModel viewModel)
        {
            foreach(EcoPointTypeHint hintEntity in entity.Details.Hints)
                viewModel.Hints.Add(new EcoPointTypeHintModel(hintEntity.Id, hintEntity.Title, hintEntity.Content, hintEntity.Url));        
        }
    }
}
