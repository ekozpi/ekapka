﻿using EkapkaBackend.Model;
using EkapkaBackend.Model.Entities;
using EkapkaBackend.Services.Base;
using EkapkaBackend.Services.EcoPointTypes.Models;

namespace EkapkaBackend.Services.EcoPointTypes.Services
{
    public class EcoPointTypesService : CrudServiceBase<EcoPointType, EcoPointTypeViewModel>, IEcoPointTypesService
    {
        public EcoPointTypesService(EkapkaDbContext dbContext) : base(dbContext)
        {
        }

        protected override void FillViewModel(EcoPointType entity, EcoPointTypeViewModel viewModel)
        {
            viewModel.Id = entity.Id;
            viewModel.Name = entity.Name;
            viewModel.Slug = entity.Slug;
        }
    }
}
