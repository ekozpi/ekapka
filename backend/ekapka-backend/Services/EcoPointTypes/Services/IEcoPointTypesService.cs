﻿using EkapkaBackend.Services.EcoPointTypes.Models;
using System.Collections.Generic;

namespace EkapkaBackend.Services.EcoPointTypes.Services
{
    public interface IEcoPointTypesService
    {
        public List<EcoPointTypeViewModel> GetAll();
        public EcoPointTypeViewModel Get(int id);
        public void Delete(int id);
    }
}
