﻿using EkapkaBackend.Services.EcoPointTypes.Models;
using System.Collections.Generic;

namespace EkapkaBackend.Services.EcoPointTypes.Services
{
    public interface IEcoPointTypeDetailsService
    {
        public List<EcoPointTypeDetailsViewModel> GetAll();
        public EcoPointTypeDetailsViewModel Get(int id);
    }
}
