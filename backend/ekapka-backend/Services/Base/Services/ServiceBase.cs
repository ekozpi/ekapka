﻿using EkapkaBackend.Model;

namespace EkapkaBackend.Services.Base
{
    public abstract class ServiceBase
    {
        public EkapkaDbContext dbContext;

        public ServiceBase(EkapkaDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
    }
}
