﻿using EkapkaBackend.Model;
using EkapkaBackend.Model.Entities;
using EkapkaBackend.Services.Base.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EkapkaBackend.Services.Base
{
    public abstract class CrudServiceBase<E, VM> : ServiceBase
        where E : class, IEntity, new()
        where VM : class, IViewModel
    {
        public CrudServiceBase(EkapkaDbContext dbContext) : base(dbContext)
        {
        }

        public List<VM> GetAll()
        {
            var entities =  dbContext.Set<E>().ToList();
            var viewModels = new List<VM>();

            foreach(var entity in entities)
            {
                var viewModel = (VM)Activator.CreateInstance(typeof(VM));
                FillViewModel(entity, viewModel);
                viewModels.Add(viewModel);
            }

            return viewModels;
        }

        public VM Get(int id)
        {
            var entity = dbContext.Set<E>().FirstOrDefault(x => x.Id == id);
            if (entity == null)
                return null;

            var viewModel = (VM)Activator.CreateInstance(typeof(VM));
            FillViewModel(entity, viewModel);

            return viewModel;
        }

        public void Delete(int id)
        {
            var entity = new E()
            {
                Id = id
            };
            dbContext.Entry(entity).State = EntityState.Deleted;
            dbContext.SaveChanges();
        }

        protected abstract void FillViewModel(E entity, VM viewModel);  
    }
}
