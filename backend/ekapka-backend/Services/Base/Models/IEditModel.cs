﻿namespace EkapkaBackend.Services.Base.Models
{
    public interface IEditModel
    {
        public int Id { get; set; }
    }
}
