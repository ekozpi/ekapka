﻿using EkapkaBackend.Services.EcoPoints.Models;
using System.Collections.Generic;

namespace EkapkaBackend.Services.EcoPoints.Services
{
    public interface IEcoPointClosestService
    {
        public List<EcoPointDistance> GetClosest(int id, int closestNumber);
    }
}
