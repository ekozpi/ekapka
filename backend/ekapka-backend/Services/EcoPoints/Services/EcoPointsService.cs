﻿using EkapkaBackend.Model;
using EkapkaBackend.Model.Entities;
using EkapkaBackend.Services.Base;
using EkapkaBackend.Services.EcoPoints.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace EkapkaBackend.Services.EcoPoints.Services
{
    public class EcoPointsService : CrudServiceBase<EcoPoint, EcoPointViewModel>, IEcoPointsService
    {
        public EcoPointsService(EkapkaDbContext dbContext) : base(dbContext)
        {
            dbContext.EcoPoints
                .Include(p => p.Address)
                .Include(p => p.Type)
                .ToList();
        }

        public void Add(EcoPointEditModel ecoPoint)
        {
            EcoPointType type = dbContext.EcoPointTypes.Find(ecoPoint.TypeId);
            EcoPoint point = new EcoPoint { Name = ecoPoint.Name, Description = ecoPoint.Description, Type = type, Longitude = ecoPoint.Longitude, Latitude = ecoPoint.Latitude, Date = DateTime.Now, Address = ecoPoint.address };
            dbContext.EcoPoints.Add(point);
            dbContext.SaveChanges();
        }

        protected override void FillViewModel(EcoPoint entity, EcoPointViewModel viewModel)
        {
            viewModel.Id = entity.Id;
            viewModel.TypeId = entity.Type.Id;
            viewModel.TypeSlug = entity.Type.Slug;
            viewModel.TypeName = entity.Type?.Name;
            viewModel.Name = entity.Name;
            viewModel.Description = entity.Description;
            viewModel.Latitude = entity.Latitude;
            viewModel.Longitude = entity.Longitude;
            viewModel.Date = entity.Date.ToShortDateString();
            viewModel.Address = entity.Address;
        }


    }
}
