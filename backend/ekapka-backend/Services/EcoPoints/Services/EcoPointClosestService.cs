﻿using EkapkaBackend.Model;
using EkapkaBackend.Model.Entities;
using EkapkaBackend.Services.Base;
using EkapkaBackend.Services.EcoPoints.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace EkapkaBackend.Services.EcoPoints.Services
{
    public class EcoPointClosestService : ServiceBase, IEcoPointClosestService
    {
        public EcoPointClosestService(EkapkaDbContext dbContext) : base(dbContext)
        {
        }

        public List<EcoPointDistance> GetClosest(int id, int closestNumber)
        {
            var allEcoPoints = dbContext.EcoPoints;
            var currentEcoPoint = allEcoPoints.FirstOrDefault(x => x.Id == id);

            if (currentEcoPoint == null)
                return null;

            var closest = new List<EcoPointDistance>();

            foreach (var ecoPoint in allEcoPoints.Where(x => x.Type.Id == currentEcoPoint.Type.Id && x.Id != currentEcoPoint.Id))
                closest.Add(
                    new EcoPointDistance(ecoPoint.Id, ecoPoint.Name, CalcDistance(currentEcoPoint, ecoPoint), ecoPoint.Type.Slug)
                );

            closest.Sort((one, another) => one.Distance.CompareTo(another.Distance));

            return closest.Count < closestNumber ? closest : closest.Take(closestNumber).ToList();
        }

        private double CalcDistance(EcoPoint one, EcoPoint another)
        {
            const double RADIUS = 6371;

            double dLon = DegToRad(another.Longitude - one.Longitude);
            double dLat = DegToRad(another.Latitude - one.Latitude);

            var a =
                Math.Sin(dLat / 2) * Math.Sin(dLat / 2) +
                Math.Cos(DegToRad(one.Latitude)) * Math.Cos(DegToRad(another.Latitude)) *
                Math.Sin(dLon / 2) * Math.Sin(dLon / 2);

            return 2 * RADIUS * Math.Asin(Math.Sqrt(a));
        }

        private double DegToRad(double x)
        {
            return x * Math.PI / 180;
        }
    }
}
