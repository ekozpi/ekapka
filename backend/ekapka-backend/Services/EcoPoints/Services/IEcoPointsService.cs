﻿using EkapkaBackend.Services.EcoPoints.Models;
using System.Collections.Generic;

namespace EkapkaBackend.Services.EcoPoints.Services
{
    public interface IEcoPointsService
    {
        public List<EcoPointViewModel> GetAll();
        public EcoPointViewModel Get(int id);
        public void Delete(int id);
        public void Add(EcoPointEditModel ecoPoint);
    }
}
