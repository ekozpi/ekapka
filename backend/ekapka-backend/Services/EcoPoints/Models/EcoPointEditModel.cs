﻿using EkapkaBackend.Model.Entities;
using EkapkaBackend.Services.Base.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EkapkaBackend.Services.EcoPoints.Models
{
    public class EcoPointEditModel : IEditModel
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Address address { get; set; }
    }
}
