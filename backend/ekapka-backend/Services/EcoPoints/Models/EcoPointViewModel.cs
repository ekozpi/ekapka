﻿using EkapkaBackend.Model.Entities;
using EkapkaBackend.Services.Base.Models;

namespace EkapkaBackend.Services.EcoPoints.Models
{
    public class EcoPointViewModel : IViewModel
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public string TypeSlug { get; set; }
        public string TypeName { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public Address Address { get; set; }
    }
}
