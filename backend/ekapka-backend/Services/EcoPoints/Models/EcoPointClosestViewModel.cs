﻿using EkapkaBackend.Services.Base.Models;
using System.Collections.Generic;

namespace EkapkaBackend.Services.EcoPoints.Models
{
    public class EcoPointClosestViewModel : IViewModel
    {
        public int Id { get; set; }
        public List<EcoPointDistance> Closest { get; set; }
    }
}
