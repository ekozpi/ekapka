﻿namespace EkapkaBackend.Services.EcoPoints.Models
{
    public class EcoPointDistance
    {
        public EcoPointDistance(int id, string name, double distance, string typeSlug)
        {
            Id = id;
            Name = name;
            Distance = distance;
            TypeSlug = typeSlug;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public double Distance { get; set; }
        public string TypeSlug { get; set; }
    }
}
