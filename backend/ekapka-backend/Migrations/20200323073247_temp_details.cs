﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class temp_details : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "EcoPoints",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "Description", "Name" },
                values: new object[] { new DateTime(2020, 3, 23, 8, 32, 46, 567, DateTimeKind.Local).AddTicks(7369), "Donec posuere quam porta ante commodo tristique. Fusce a malesuada velit. In accumsan elit quis euismod scelerisque. Mauris lacinia accumsan efficitur. Donec ornare tristique sodales. Vivamus vehicula metus ut metus consequat, venenatis semper nisi condimentum. Donec et rutrum arcu. Aenean pharetra nunc vitae justo convallis tristique. Nulla venenatis tortor quis venenatis commodo. Morbi ac diam at purus iaculis tincidunt non in nunc.", "Lorem ipsum" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "Description", "Name" },
                values: new object[] { new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(8879), "Mauris blandit erat at dolor vehicula, sit amet fermentum ligula molestie. Curabitur blandit sagittis nibh, vitae auctor enim porta ac. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis et iaculis elit. Vestibulum et leo vitae magna varius sollicitudin ac nec erat. In lacinia urna ac sapien egestas sagittis. Nulla tellus magna, lacinia in turpis non, molestie varius felis. Vestibulum ultricies felis sit amet sem semper, at ullamcorper ligula tempor. Morbi finibus luctus turpis congue ullamcorper. Nullam nec enim ornare, ultricies magna fringilla, placerat tellus.", "Dolor sit amet" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "Description", "Name" },
                values: new object[] { new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(9014), "Pellentesque sit amet felis placerat, pulvinar enim nec, euismod est. Nunc molestie libero condimentum bibendum scelerisque. Quisque ut tempor ligula, nec aliquam nulla. Integer convallis urna fermentum diam cursus, eget tincidunt urna hendrerit. Proin sollicitudin sollicitudin orci eu tempus. Duis congue nunc eget arcu molestie, at egestas nibh euismod. Donec et turpis lacus. Proin bibendum, ex sit amet vulputate fringilla, lectus sapien malesuada nisi, eu posuere nulla sapien ac turpis. Curabitur semper leo nec pellentesque rutrum. Fusce tempor mollis aliquet.", "Lorem dolor" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "Description", "Name" },
                values: new object[] { new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(9023), "Curabitur eget est ut tortor aliquet aliquet vitae ut nulla. Donec nibh mi, ullamcorper et neque eget, cursus rhoncus nulla. Suspendisse sit amet nulla ut felis commodo cursus. Nulla magna augue, feugiat sed ullamcorper eu, pulvinar sit amet lorem. Etiam egestas, turpis quis ornare laoreet, est lacus tincidunt ex, a maximus lorem neque molestie eros. Proin nec vestibulum magna. Mauris vehicula, nisi ac lacinia elementum, libero dui ornare dolor, quis lobortis lectus odio at turpis. Aenean imperdiet sodales nisi, in faucibus nunc pulvinar vitae. Nulla sapien lacus, mattis eget est quis, sollicitudin molestie purus. Vivamus euismod scelerisque congue.", "Homo sum" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "Description", "Name" },
                values: new object[] { new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(9028), "Curabitur vitae facilisis turpis. Nunc ultricies augue augue, quis mollis lacus tempus vel. Praesent et euismod nulla. In turpis turpis, dapibus quis ipsum ac, finibus tincidunt tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean elit neque, feugiat nec euismod sit amet, sagittis in odio. Vestibulum dapibus hendrerit augue, non iaculis magna accumsan ac. Nullam vel enim et massa condimentum consectetur at eget turpis. Vivamus tempus tristique mi, ut euismod nunc dapibus non. Integer tortor sapien, bibendum sit amet malesuada sed, hendrerit in elit.", "Et nihil humanum" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "Description", "Name" },
                values: new object[] { new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(9038), "Etiam urna odio, sollicitudin ac erat molestie, convallis faucibus urna. Integer tincidunt, libero luctus commodo viverra, turpis orci iaculis nibh, vel mollis sapien enim sed nibh. Nunc sit amet nunc condimentum odio finibus malesuada in vitae erat. Mauris ut lacus sed diam varius fringilla eget nec nibh. In dapibus odio dolor, vitae bibendum libero varius vitae. Etiam neque urna, congue non nibh sit amet, congue pellentesque nunc. In non sagittis dolor, elementum fermentum risus. Quisque volutpat felis ut leo elementum, gravida mollis libero vulputate.", "Ame alienum" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Date", "Description", "Name" },
                values: new object[] { new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(9044), "In euismod euismod metus, et dapibus urna eleifend vel. Sed sit amet urna et sem pretium mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer viverra dolor est, at condimentum lacus tempus et. Mauris finibus leo malesuada nulla ornare pretium. Suspendisse eget euismod libero. Suspendisse vel mattis risus, in vulputate ipsum. Aenean euismod purus lorem, in pellentesque turpis pellentesque a. Quisque vitae porta urna.", "Esse puto" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "EcoPoints");

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Date", "Description" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 245, DateTimeKind.Local).AddTicks(7731), "Lorem ipsum" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "Description" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2835), "Dolor sit amet" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "Description" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2932), "Lorem dolor" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "Description" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2939), "Homo sum" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "Description" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2944), "Et nihil humanum" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "Description" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2957), "Ame alienum" });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Date", "Description" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2961), "Esse puto" });
        }
    }
}
