﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class ecopointdetailshints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPointTypeHint_EcoPointTypeDetails_DetailsId",
                table: "EcoPointTypeHint");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EcoPointTypeHint",
                table: "EcoPointTypeHint");

            migrationBuilder.RenameTable(
                name: "EcoPointTypeHint",
                newName: "EcoPointTypeHints");

            migrationBuilder.RenameIndex(
                name: "IX_EcoPointTypeHint_DetailsId",
                table: "EcoPointTypeHints",
                newName: "IX_EcoPointTypeHints_DetailsId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EcoPointTypeHints",
                table: "EcoPointTypeHints",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPointTypeHints_EcoPointTypeDetails_DetailsId",
                table: "EcoPointTypeHints",
                column: "DetailsId",
                principalTable: "EcoPointTypeDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPointTypeHints_EcoPointTypeDetails_DetailsId",
                table: "EcoPointTypeHints");

            migrationBuilder.DropPrimaryKey(
                name: "PK_EcoPointTypeHints",
                table: "EcoPointTypeHints");

            migrationBuilder.RenameTable(
                name: "EcoPointTypeHints",
                newName: "EcoPointTypeHint");

            migrationBuilder.RenameIndex(
                name: "IX_EcoPointTypeHints_DetailsId",
                table: "EcoPointTypeHint",
                newName: "IX_EcoPointTypeHint_DetailsId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EcoPointTypeHint",
                table: "EcoPointTypeHint",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPointTypeHint_EcoPointTypeDetails_DetailsId",
                table: "EcoPointTypeHint",
                column: "DetailsId",
                principalTable: "EcoPointTypeDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
