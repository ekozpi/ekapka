﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class addurltohints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "EcoPointTypeHint",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Url",
                table: "EcoPointTypeHint");
        }
    }
}
