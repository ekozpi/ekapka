﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class Addpredefinedaddressinformation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AddressId",
                table: "EcoPoints",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Address",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Country = table.Column<string>(nullable: true),
                    State = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    Street = table.Column<string>(nullable: true),
                    Building = table.Column<string>(nullable: true),
                    Flat = table.Column<string>(nullable: true),
                    Postcode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Address", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Address",
                columns: new[] { "Id", "Building", "City", "Country", "Flat", "Postcode", "State", "Street" },
                values: new object[,]
                {
                    { 1, "Szkoła Podstawowa nr 97", "Wrocław", "Polska", "", "53-509", "województwo dolnośląskie", "Prosta" },
                    { 2, "37", "Wrocław", "Polska", "", "50-149", "województwo dolnośląskie", "Wita Stwosza" },
                    { 3, "8", "Wrocław", "Polska", "", "50-122", "województwo dolnośląskie", "Szewska" },
                    { 4, "6", "Wrocław", "Polska", "", "50-159", "województwo dolnośląskie", "Plac Dominikański" },
                    { 5, "Kompleks Sportowy IX LO i Liceum Plastycznego", "Wrocław", "Polska", "", "50-124", "województwo dolnośląskie", "Nowa" },
                    { 6, "Jednostka Ratowniczo-Gaśnicza PSP nr 1 we Wrocławiu", "Wrocław", "Polska", "", "50-056", "województwo dolnośląskie", "Mennicza" },
                    { 7, "73", "Wrocław", "Polska", "", "50-449", "województwo dolnośląskie", "Podwale" }
                });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddressId", "Date" },
                values: new object[] { 1, new DateTime(2020, 4, 9, 21, 48, 37, 265, DateTimeKind.Local).AddTicks(907) });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddressId", "Date" },
                values: new object[] { 2, new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9171) });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddressId", "Date" },
                values: new object[] { 3, new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9414) });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddressId", "Date" },
                values: new object[] { 4, new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9426) });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddressId", "Date" },
                values: new object[] { 5, new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9432) });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "AddressId", "Date" },
                values: new object[] { 6, new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9444) });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "AddressId", "Date" },
                values: new object[] { 7, new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9452) });

            migrationBuilder.CreateIndex(
                name: "IX_EcoPoints_AddressId",
                table: "EcoPoints",
                column: "AddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPoints_Address_AddressId",
                table: "EcoPoints",
                column: "AddressId",
                principalTable: "Address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPoints_Address_AddressId",
                table: "EcoPoints");

            migrationBuilder.DropTable(
                name: "Address");

            migrationBuilder.DropIndex(
                name: "IX_EcoPoints_AddressId",
                table: "EcoPoints");

            migrationBuilder.DropColumn(
                name: "AddressId",
                table: "EcoPoints");

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 3, 23, 8, 32, 46, 567, DateTimeKind.Local).AddTicks(7369));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(8879));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(9014));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(9023));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(9028));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(9038));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                column: "Date",
                value: new DateTime(2020, 3, 23, 8, 32, 46, 572, DateTimeKind.Local).AddTicks(9044));
        }
    }
}
