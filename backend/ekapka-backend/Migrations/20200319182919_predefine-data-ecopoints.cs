﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class predefinedataecopoints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "EcoPoints",
                columns: new[] { "Id", "Date", "Description", "Latitude", "Longitude", "TypeId" },
                values: new object[,]
                {
                    { 1, new DateTime(2020, 3, 19, 19, 29, 19, 177, DateTimeKind.Local).AddTicks(9526), "Lorem ipsum", 51.104532300000002, 17.0189755, 1 },
                    { 2, new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8506), "Dolor sit amet", 51.104532300000002, 17.0189755, 2 },
                    { 3, new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8709), "Lorem dolor", 51.104532300000002, 17.0189755, 3 },
                    { 4, new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8722), "Homo sum", 51.104532300000002, 17.0189755, 4 },
                    { 5, new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8731), "Et nihil humanum", 51.104532300000002, 17.0189755, 5 },
                    { 6, new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8748), "Ame alienum", 51.104532300000002, 17.0189755, 6 },
                    { 7, new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8756), "Esse puto", 51.104532300000002, 17.0189755, 7 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7);
        }
    }
}
