﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class categoriesdetailsadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DetailsId",
                table: "EcoPointTypes",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "EcoPointTypeDetails",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcoPointTypeDetails", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EcoPointTypeHint",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    EcoPointTypeDetailsId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcoPointTypeHint", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EcoPointTypeHint_EcoPointTypeDetails_EcoPointTypeDetailsId",
                        column: x => x.EcoPointTypeDetailsId,
                        principalTable: "EcoPointTypeDetails",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EcoPointTypes_DetailsId",
                table: "EcoPointTypes",
                column: "DetailsId");

            migrationBuilder.CreateIndex(
                name: "IX_EcoPointTypeHint_EcoPointTypeDetailsId",
                table: "EcoPointTypeHint",
                column: "EcoPointTypeDetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPointTypes_EcoPointTypeDetails_DetailsId",
                table: "EcoPointTypes",
                column: "DetailsId",
                principalTable: "EcoPointTypeDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPointTypes_EcoPointTypeDetails_DetailsId",
                table: "EcoPointTypes");

            migrationBuilder.DropTable(
                name: "EcoPointTypeHint");

            migrationBuilder.DropTable(
                name: "EcoPointTypeDetails");

            migrationBuilder.DropIndex(
                name: "IX_EcoPointTypes_DetailsId",
                table: "EcoPointTypes");

            migrationBuilder.DropColumn(
                name: "DetailsId",
                table: "EcoPointTypes");
        }
    }
}
