﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class changecoordinatesofpredefinedecopoints : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 3, 22, 21, 7, 54, 373, DateTimeKind.Local).AddTicks(1012));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3139), 51.110092999999999, 17.036493 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3271), 51.108843, 17.034237999999998 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3279), 51.109375, 17.041312999999999 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3284), 51.106274999999997, 17.039415000000002 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3295), 51.105359, 17.035648999999999 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3299), 51.105513999999999, 17.041785000000001 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 3, 19, 19, 29, 19, 177, DateTimeKind.Local).AddTicks(9526));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8506), 51.104532300000002, 17.0189755 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8709), 51.104532300000002, 17.0189755 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8722), 51.104532300000002, 17.0189755 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8731), 51.104532300000002, 17.0189755 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8748), 51.104532300000002, 17.0189755 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Date", "Latitude", "Longitude" },
                values: new object[] { new DateTime(2020, 3, 19, 19, 29, 19, 182, DateTimeKind.Local).AddTicks(8756), 51.104532300000002, 17.0189755 });
        }
    }
}
