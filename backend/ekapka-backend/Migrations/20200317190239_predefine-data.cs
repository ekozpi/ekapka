﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class predefinedata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.UpdateData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Content",
                value: "Zużyte baterie");

            migrationBuilder.InsertData(
                table: "Texts",
                columns: new[] { "Id", "Content" },
                values: new object[,]
                {
                    { 2, "Niepotrzebne ubrania" },
                    { 3, "Przeterminowane leki" },
                    { 4, "Skup surowców wtórnych" },
                    { 5, "Jadłodzielnia" },
                    { 6, "Pojemniki recyklingowe" },
                    { 7, "Zbiórki" }
                });

            migrationBuilder.InsertData(
                table: "EcoPointTypes",
                columns: new[] { "Id", "NameId" },
                values: new object[,]
                {
                    { 2, 2 },
                    { 3, 3 },
                    { 4, 4 },
                    { 5, 5 },
                    { 6, 6 },
                    { 7, 7 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.InsertData(
                table: "EcoPoints",
                columns: new[] { "Id", "Date", "Description", "Latitude", "Longitude", "TypeId" },
                values: new object[] { 1, new DateTime(2020, 3, 15, 1, 4, 19, 424, DateTimeKind.Local).AddTicks(6900), "Galeria Dominikańska", 51.107885199999998, 17.038537600000002, 1 });

            migrationBuilder.UpdateData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Content",
                value: "Recycling bins");
        }
    }
}
