﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class temp_predefine_data : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 3, 23, 7, 38, 52, 245, DateTimeKind.Local).AddTicks(7731));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2835));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2932));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "TypeId" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2939), 1 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "TypeId" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2944), 2 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "TypeId" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2957), 3 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Date", "TypeId" },
                values: new object[] { new DateTime(2020, 3, 23, 7, 38, 52, 249, DateTimeKind.Local).AddTicks(2961), 4 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 3, 22, 21, 7, 54, 373, DateTimeKind.Local).AddTicks(1012));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3139));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3271));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Date", "TypeId" },
                values: new object[] { new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3279), 4 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "Date", "TypeId" },
                values: new object[] { new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3284), 5 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "Date", "TypeId" },
                values: new object[] { new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3295), 6 });

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "Date", "TypeId" },
                values: new object[] { new DateTime(2020, 3, 22, 21, 7, 54, 376, DateTimeKind.Local).AddTicks(3299), 7 });
        }
    }
}
