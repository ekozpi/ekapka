﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class predefinedatachanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPoints_EcoPointTypes_TypeId",
                table: "EcoPoints");

            migrationBuilder.DropForeignKey(
                name: "FK_EcoPointTypes_Texts_NameId",
                table: "EcoPointTypes");

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Address",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Address",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Address",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Address",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Address",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Address",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Address",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "EcoPointTypes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Texts",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.AlterColumn<int>(
                name: "NameId",
                table: "EcoPointTypes",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "TypeId",
                table: "EcoPoints",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPoints_EcoPointTypes_TypeId",
                table: "EcoPoints",
                column: "TypeId",
                principalTable: "EcoPointTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPointTypes_Texts_NameId",
                table: "EcoPointTypes",
                column: "NameId",
                principalTable: "Texts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPoints_EcoPointTypes_TypeId",
                table: "EcoPoints");

            migrationBuilder.DropForeignKey(
                name: "FK_EcoPointTypes_Texts_NameId",
                table: "EcoPointTypes");

            migrationBuilder.AlterColumn<int>(
                name: "NameId",
                table: "EcoPointTypes",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TypeId",
                table: "EcoPoints",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.InsertData(
                table: "Address",
                columns: new[] { "Id", "Building", "City", "Country", "Flat", "Postcode", "State", "Street" },
                values: new object[,]
                {
                    { 1, "Szkoła Podstawowa nr 97", "Wrocław", "Polska", "", "53-509", "województwo dolnośląskie", "Prosta" },
                    { 2, "37", "Wrocław", "Polska", "", "50-149", "województwo dolnośląskie", "Wita Stwosza" },
                    { 3, "8", "Wrocław", "Polska", "", "50-122", "województwo dolnośląskie", "Szewska" },
                    { 4, "6", "Wrocław", "Polska", "", "50-159", "województwo dolnośląskie", "Plac Dominikański" },
                    { 5, "Kompleks Sportowy IX LO i Liceum Plastycznego", "Wrocław", "Polska", "", "50-124", "województwo dolnośląskie", "Nowa" },
                    { 6, "Jednostka Ratowniczo-Gaśnicza PSP nr 1 we Wrocławiu", "Wrocław", "Polska", "", "50-056", "województwo dolnośląskie", "Mennicza" },
                    { 7, "73", "Wrocław", "Polska", "", "50-449", "województwo dolnośląskie", "Podwale" }
                });

            migrationBuilder.InsertData(
                table: "Texts",
                columns: new[] { "Id", "Content" },
                values: new object[,]
                {
                    { 1, "Zużyte baterie" },
                    { 2, "Niepotrzebne ubrania" },
                    { 3, "Przeterminowane leki" },
                    { 4, "Skup surowców wtórnych" },
                    { 5, "Jadłodzielnia" },
                    { 6, "Pojemniki recyklingowe" },
                    { 7, "Zbiórki" }
                });

            migrationBuilder.InsertData(
                table: "EcoPointTypes",
                columns: new[] { "Id", "NameId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 2 },
                    { 3, 3 },
                    { 4, 4 },
                    { 5, 5 },
                    { 6, 6 },
                    { 7, 7 }
                });

            migrationBuilder.InsertData(
                table: "EcoPoints",
                columns: new[] { "Id", "AddressId", "Date", "Description", "Latitude", "Longitude", "Name", "TypeId" },
                values: new object[,]
                {
                    { 1, 1, new DateTime(2020, 4, 9, 22, 32, 13, 450, DateTimeKind.Local).AddTicks(4451), "Donec posuere quam porta ante commodo tristique. Fusce a malesuada velit. In accumsan elit quis euismod scelerisque. Mauris lacinia accumsan efficitur. Donec ornare tristique sodales. Vivamus vehicula metus ut metus consequat, venenatis semper nisi condimentum. Donec et rutrum arcu. Aenean pharetra nunc vitae justo convallis tristique. Nulla venenatis tortor quis venenatis commodo. Morbi ac diam at purus iaculis tincidunt non in nunc.", 51.104532300000002, 17.0189755, "Lorem ipsum", 1 },
                    { 4, 4, new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6730), "Curabitur eget est ut tortor aliquet aliquet vitae ut nulla. Donec nibh mi, ullamcorper et neque eget, cursus rhoncus nulla. Suspendisse sit amet nulla ut felis commodo cursus. Nulla magna augue, feugiat sed ullamcorper eu, pulvinar sit amet lorem. Etiam egestas, turpis quis ornare laoreet, est lacus tincidunt ex, a maximus lorem neque molestie eros. Proin nec vestibulum magna. Mauris vehicula, nisi ac lacinia elementum, libero dui ornare dolor, quis lobortis lectus odio at turpis. Aenean imperdiet sodales nisi, in faucibus nunc pulvinar vitae. Nulla sapien lacus, mattis eget est quis, sollicitudin molestie purus. Vivamus euismod scelerisque congue.", 51.109375, 17.041312999999999, "Homo sum", 1 },
                    { 2, 2, new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6032), "Mauris blandit erat at dolor vehicula, sit amet fermentum ligula molestie. Curabitur blandit sagittis nibh, vitae auctor enim porta ac. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis et iaculis elit. Vestibulum et leo vitae magna varius sollicitudin ac nec erat. In lacinia urna ac sapien egestas sagittis. Nulla tellus magna, lacinia in turpis non, molestie varius felis. Vestibulum ultricies felis sit amet sem semper, at ullamcorper ligula tempor. Morbi finibus luctus turpis congue ullamcorper. Nullam nec enim ornare, ultricies magna fringilla, placerat tellus.", 51.110092999999999, 17.036493, "Dolor sit amet", 2 },
                    { 5, 5, new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6750), "Curabitur vitae facilisis turpis. Nunc ultricies augue augue, quis mollis lacus tempus vel. Praesent et euismod nulla. In turpis turpis, dapibus quis ipsum ac, finibus tincidunt tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean elit neque, feugiat nec euismod sit amet, sagittis in odio. Vestibulum dapibus hendrerit augue, non iaculis magna accumsan ac. Nullam vel enim et massa condimentum consectetur at eget turpis. Vivamus tempus tristique mi, ut euismod nunc dapibus non. Integer tortor sapien, bibendum sit amet malesuada sed, hendrerit in elit.", 51.106274999999997, 17.039415000000002, "Et nihil humanum", 2 },
                    { 3, 3, new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6705), "Pellentesque sit amet felis placerat, pulvinar enim nec, euismod est. Nunc molestie libero condimentum bibendum scelerisque. Quisque ut tempor ligula, nec aliquam nulla. Integer convallis urna fermentum diam cursus, eget tincidunt urna hendrerit. Proin sollicitudin sollicitudin orci eu tempus. Duis congue nunc eget arcu molestie, at egestas nibh euismod. Donec et turpis lacus. Proin bibendum, ex sit amet vulputate fringilla, lectus sapien malesuada nisi, eu posuere nulla sapien ac turpis. Curabitur semper leo nec pellentesque rutrum. Fusce tempor mollis aliquet.", 51.108843, 17.034237999999998, "Lorem dolor", 3 },
                    { 6, 6, new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6779), "Etiam urna odio, sollicitudin ac erat molestie, convallis faucibus urna. Integer tincidunt, libero luctus commodo viverra, turpis orci iaculis nibh, vel mollis sapien enim sed nibh. Nunc sit amet nunc condimentum odio finibus malesuada in vitae erat. Mauris ut lacus sed diam varius fringilla eget nec nibh. In dapibus odio dolor, vitae bibendum libero varius vitae. Etiam neque urna, congue non nibh sit amet, congue pellentesque nunc. In non sagittis dolor, elementum fermentum risus. Quisque volutpat felis ut leo elementum, gravida mollis libero vulputate.", 51.105359, 17.035648999999999, "Ame alienum", 3 },
                    { 7, 7, new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6787), "In euismod euismod metus, et dapibus urna eleifend vel. Sed sit amet urna et sem pretium mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer viverra dolor est, at condimentum lacus tempus et. Mauris finibus leo malesuada nulla ornare pretium. Suspendisse eget euismod libero. Suspendisse vel mattis risus, in vulputate ipsum. Aenean euismod purus lorem, in pellentesque turpis pellentesque a. Quisque vitae porta urna.", 51.105513999999999, 17.041785000000001, "Esse puto", 4 }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPoints_EcoPointTypes_TypeId",
                table: "EcoPoints",
                column: "TypeId",
                principalTable: "EcoPointTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPointTypes_Texts_NameId",
                table: "EcoPointTypes",
                column: "NameId",
                principalTable: "Texts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
