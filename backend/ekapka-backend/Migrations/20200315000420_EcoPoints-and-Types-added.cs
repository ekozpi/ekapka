﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class EcoPointsandTypesadded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WeatherForecasts");

            migrationBuilder.CreateTable(
                name: "Texts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Texts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EcoPointTypes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NameId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcoPointTypes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EcoPointTypes_Texts_NameId",
                        column: x => x.NameId,
                        principalTable: "Texts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EcoPoints",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TypeId = table.Column<int>(nullable: false),
                    Longitude = table.Column<double>(nullable: false),
                    Latitude = table.Column<double>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EcoPoints", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EcoPoints_EcoPointTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "EcoPointTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Texts",
                columns: new[] { "Id", "Content" },
                values: new object[] { 1, "Recycling bins" });

            migrationBuilder.InsertData(
                table: "EcoPointTypes",
                columns: new[] { "Id", "NameId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "EcoPoints",
                columns: new[] { "Id", "Date", "Description", "Latitude", "Longitude", "TypeId" },
                values: new object[] { 1, new DateTime(2020, 3, 15, 1, 4, 19, 424, DateTimeKind.Local).AddTicks(6900), "Galeria Dominikańska", 51.107885199999998, 17.038537600000002, 1 });

            migrationBuilder.CreateIndex(
                name: "IX_EcoPoints_TypeId",
                table: "EcoPoints",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_EcoPointTypes_NameId",
                table: "EcoPointTypes",
                column: "NameId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EcoPoints");

            migrationBuilder.DropTable(
                name: "EcoPointTypes");

            migrationBuilder.DropTable(
                name: "Texts");

            migrationBuilder.CreateTable(
                name: "WeatherForecasts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Date = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Temperature = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WeatherForecasts", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "WeatherForecasts",
                columns: new[] { "Id", "Date", "Temperature" },
                values: new object[] { 1, "22.02.2019", 12 });
        }
    }
}
