﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class ChangeaddresscolumninEcoPointtonullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPoints_Address_AddressId",
                table: "EcoPoints");

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "EcoPoints",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 450, DateTimeKind.Local).AddTicks(4451));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6032));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6705));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6730));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6750));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6779));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6787));

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPoints_Address_AddressId",
                table: "EcoPoints",
                column: "AddressId",
                principalTable: "Address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPoints_Address_AddressId",
                table: "EcoPoints");

            migrationBuilder.AlterColumn<int>(
                name: "AddressId",
                table: "EcoPoints",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 4, 9, 21, 48, 37, 265, DateTimeKind.Local).AddTicks(907));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9171));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9414));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9426));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9432));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9444));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                column: "Date",
                value: new DateTime(2020, 4, 9, 21, 48, 37, 268, DateTimeKind.Local).AddTicks(9452));

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPoints_Address_AddressId",
                table: "EcoPoints",
                column: "AddressId",
                principalTable: "Address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
