﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class removetextentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPointTypes_Texts_NameId",
                table: "EcoPointTypes");

            migrationBuilder.DropTable(
                name: "Texts");

            migrationBuilder.DropIndex(
                name: "IX_EcoPointTypes_NameId",
                table: "EcoPointTypes");

            migrationBuilder.DropColumn(
                name: "NameId",
                table: "EcoPointTypes");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "EcoPointTypes",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "EcoPointTypes");

            migrationBuilder.AddColumn<int>(
                name: "NameId",
                table: "EcoPointTypes",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Texts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Texts", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EcoPointTypes_NameId",
                table: "EcoPointTypes",
                column: "NameId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPointTypes_Texts_NameId",
                table: "EcoPointTypes",
                column: "NameId",
                principalTable: "Texts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
