﻿// <auto-generated />
using System;
using EkapkaBackend.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace EkapkaBackend.Migrations
{
    [DbContext(typeof(EkapkaDbContext))]
    [Migration("20200315000420_EcoPoints-and-Types-added")]
    partial class EcoPointsandTypesadded
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.2")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EkapkaBackend.Model.Entities.EcoPoint", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("Date")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<double>("Latitude")
                        .HasColumnType("float");

                    b.Property<double>("Longitude")
                        .HasColumnType("float");

                    b.Property<int>("TypeId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TypeId");

                    b.ToTable("EcoPoints");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Date = new DateTime(2020, 3, 15, 1, 4, 19, 424, DateTimeKind.Local).AddTicks(6900),
                            Description = "Galeria Dominikańska",
                            Latitude = 51.107885199999998,
                            Longitude = 17.038537600000002,
                            TypeId = 1
                        });
                });

            modelBuilder.Entity("EkapkaBackend.Model.Entities.EcoPointType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("NameId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("NameId");

                    b.ToTable("EcoPointTypes");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            NameId = 1
                        });
                });

            modelBuilder.Entity("EkapkaBackend.Model.Entities.Text", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Content")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Texts");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            Content = "Recycling bins"
                        });
                });

            modelBuilder.Entity("EkapkaBackend.Model.Entities.EcoPoint", b =>
                {
                    b.HasOne("EkapkaBackend.Model.Entities.EcoPointType", "Type")
                        .WithMany()
                        .HasForeignKey("TypeId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("EkapkaBackend.Model.Entities.EcoPointType", b =>
                {
                    b.HasOne("EkapkaBackend.Model.Entities.Text", "Name")
                        .WithMany()
                        .HasForeignKey("NameId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
