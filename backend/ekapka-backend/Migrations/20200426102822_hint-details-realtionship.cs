﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class hintdetailsrealtionship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPointTypeHint_EcoPointTypeDetails_EcoPointTypeDetailsId",
                table: "EcoPointTypeHint");

            migrationBuilder.DropIndex(
                name: "IX_EcoPointTypeHint_EcoPointTypeDetailsId",
                table: "EcoPointTypeHint");

            migrationBuilder.DropColumn(
                name: "EcoPointTypeDetailsId",
                table: "EcoPointTypeHint");

            migrationBuilder.AddColumn<int>(
                name: "DetailsId",
                table: "EcoPointTypeHint",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EcoPointTypeHint_DetailsId",
                table: "EcoPointTypeHint",
                column: "DetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPointTypeHint_EcoPointTypeDetails_DetailsId",
                table: "EcoPointTypeHint",
                column: "DetailsId",
                principalTable: "EcoPointTypeDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EcoPointTypeHint_EcoPointTypeDetails_DetailsId",
                table: "EcoPointTypeHint");

            migrationBuilder.DropIndex(
                name: "IX_EcoPointTypeHint_DetailsId",
                table: "EcoPointTypeHint");

            migrationBuilder.DropColumn(
                name: "DetailsId",
                table: "EcoPointTypeHint");

            migrationBuilder.AddColumn<int>(
                name: "EcoPointTypeDetailsId",
                table: "EcoPointTypeHint",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EcoPointTypeHint_EcoPointTypeDetailsId",
                table: "EcoPointTypeHint",
                column: "EcoPointTypeDetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_EcoPointTypeHint_EcoPointTypeDetails_EcoPointTypeDetailsId",
                table: "EcoPointTypeHint",
                column: "EcoPointTypeDetailsId",
                principalTable: "EcoPointTypeDetails",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
