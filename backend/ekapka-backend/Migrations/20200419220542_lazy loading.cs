﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EkapkaBackend.Migrations
{
    public partial class lazyloading : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 4, 20, 0, 5, 41, 353, DateTimeKind.Local).AddTicks(1873));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 4, 20, 0, 5, 41, 357, DateTimeKind.Local).AddTicks(5673));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 4, 20, 0, 5, 41, 357, DateTimeKind.Local).AddTicks(5818));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 4, 20, 0, 5, 41, 357, DateTimeKind.Local).AddTicks(5828));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 4, 20, 0, 5, 41, 357, DateTimeKind.Local).AddTicks(5833));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 4, 20, 0, 5, 41, 357, DateTimeKind.Local).AddTicks(5843));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                column: "Date",
                value: new DateTime(2020, 4, 20, 0, 5, 41, 357, DateTimeKind.Local).AddTicks(5848));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 1,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 450, DateTimeKind.Local).AddTicks(4451));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 2,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6032));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 3,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6705));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 4,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6730));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 5,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6750));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 6,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6779));

            migrationBuilder.UpdateData(
                table: "EcoPoints",
                keyColumn: "Id",
                keyValue: 7,
                column: "Date",
                value: new DateTime(2020, 4, 9, 22, 32, 13, 454, DateTimeKind.Local).AddTicks(6787));
        }
    }
}
