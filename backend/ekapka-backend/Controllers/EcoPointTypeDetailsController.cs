﻿using EkapkaBackend.Model;
using EkapkaBackend.Services.EcoPointTypes.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace EkapkaBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EcoPointTypeDetailsController : EkapkaControllerBase
    {
        private readonly IEcoPointTypeDetailsService _ecoPointTypeDetailsService;

        public EcoPointTypeDetailsController(
            IEcoPointTypeDetailsService ecoPointTypeDetailsService,
            EkapkaDbContext dbContext
        ) : base(dbContext)
        {
            _ecoPointTypeDetailsService = ecoPointTypeDetailsService;
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            try
            {
                var ecoPointTypeDetailsList = _ecoPointTypeDetailsService.GetAll();
                return Ok(ecoPointTypeDetailsList);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            try
            {
                var ecoPointTypeDetails = _ecoPointTypeDetailsService.Get(id);
                return Ok(ecoPointTypeDetails);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
