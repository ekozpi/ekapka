﻿using EkapkaBackend.Model;
using EkapkaBackend.Services.EcoPointTypes.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace EkapkaBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class EcoPointTypesController : EkapkaControllerBase
    {
        private readonly IEcoPointTypesService _ecoPointTypesService;

        public EcoPointTypesController(
            EkapkaDbContext dbContext,
            IEcoPointTypesService ecoPointTypesService
            ) : base(dbContext)
        {
            _ecoPointTypesService = ecoPointTypesService;
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            try
            {
                var ecoPointTypes = _ecoPointTypesService.GetAll();
                return Ok(ecoPointTypes);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
