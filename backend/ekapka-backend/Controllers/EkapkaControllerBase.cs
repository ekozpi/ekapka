﻿using EkapkaBackend.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace EkapkaBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors]
    public abstract class EkapkaControllerBase : ControllerBase 
    {
        public EkapkaDbContext dbContext;

        public EkapkaControllerBase(EkapkaDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
    }
}
