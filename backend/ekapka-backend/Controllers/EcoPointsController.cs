﻿using EkapkaBackend.Model;
using EkapkaBackend.Services.EcoPoints.Models;
using EkapkaBackend.Services.EcoPoints.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace EkapkaBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [EnableCors]
    public class EcoPointsController : EkapkaControllerBase
    {
        private readonly IEcoPointClosestService _ecoPointClosestService;
        private readonly IEcoPointsService _ecoPointsService;

        public EcoPointsController(
            EkapkaDbContext dbContext, 
            IEcoPointClosestService ecoPointClosestService,
            IEcoPointsService ecoPointsService
            ) : base(dbContext)
        {
            _ecoPointClosestService = ecoPointClosestService;
            _ecoPointsService = ecoPointsService;
        }

        [HttpGet("closest/")]
        public ActionResult<List<EcoPointDistance>> GetClosest([FromQuery]int id, [FromQuery]int closestNumber)
        {
            var closestPoints = _ecoPointClosestService.GetClosest(id, closestNumber);

            if (closestPoints == null)
                return BadRequest();

            return Ok(closestPoints);
        }

        [HttpGet]
        public ActionResult GetAll()
        {
            try
            {
                var ecoPoints = _ecoPointsService.GetAll();
                return Ok(ecoPoints);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            try
            {
                var ecoPoint = _ecoPointsService.Get(id);
                return Ok(ecoPoint);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _ecoPointsService.Delete(id);
                return Ok();
            } catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPost]
        public ActionResult Add([FromBody]EcoPointEditModel ecoPoint)
        {

            try
            {
                _ecoPointsService.Add(ecoPoint);
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}



