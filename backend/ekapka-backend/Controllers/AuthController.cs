﻿using EkapkaBackend.Services.Auth;
using EkapkaBackend.Services.Auth.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using Microsoft.AspNetCore.Authentication;

namespace EkapkaBackend.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    [EnableCors]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService) : base() {
            _authService = authService;
        }

        [HttpPost]
        public async Task<ActionResult> Register([FromBody]RegisterEditModel model)
        {
            try
            {
                IdentityUser user;
                var result = _authService.Register(model, out user);
                
                if (result.Succeeded)
                {
                    _authService.GenerateConfirmationLinkAndSendEmail(user, Url, Request);
                    return Ok();
                }

                return BadRequest(result.Errors.First().Description);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        public async Task<ActionResult> VerifyEmail(string userId, string token)
        {
            var result = _authService.ConfirmEmail(userId, token).Result;

            if (result.Succeeded)
            {
                return Redirect("http://localhost:3000/users/login");
            }
            
            return BadRequest();
        }

        [HttpPost]
        public ActionResult LogIn([FromBody]LoginEditModel model)
        {
            try
            {
                var result = _authService.LogIn(model);

                if (result.Succeeded)
                    return Ok();

                if (result.RequiresTwoFactor)
                    return BadRequest("Two factor authentication needed");

                if (result.IsLockedOut)
                    return BadRequest("User locked out");

                if (result.IsNotAllowed)
                    return BadRequest("User not allowed");

                return BadRequest("Something went wrong");
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpPost]
        public async Task<ActionResult> LogOut()
        {
            try
            {
                _authService.LogOut();

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
