﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EkapkaBackend.Model.Entities
{
    public class EcoPointType : IEntity
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }

        public EcoPointTypeDetails Details { get; set; }
        public ICollection<EcoPoint> EcoPoints { get; set; }
    }
}
