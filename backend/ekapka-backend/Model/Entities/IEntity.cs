﻿namespace EkapkaBackend.Model.Entities
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
