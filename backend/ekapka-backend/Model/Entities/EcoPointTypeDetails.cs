﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EkapkaBackend.Model.Entities
{
    public class EcoPointTypeDetails : IEntity
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Description { get; set; }

        public ICollection<EcoPointTypeHint> Hints { get; set; }
    }
}
