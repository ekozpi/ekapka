﻿using EkapkaBackend.Model.Entities;
using Microsoft.EntityFrameworkCore;
using EkapkaBackend.Model.ModelBuilderExtensions;
using System.Reflection;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace EkapkaBackend.Model
{
    public class EkapkaDbContext : IdentityDbContext
    {
        public EkapkaDbContext(DbContextOptions<EkapkaDbContext> options) : base(options) { }

        public DbSet<EcoPointType> EcoPointTypes { get; set; }
        public DbSet<EcoPoint> EcoPoints { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<EcoPointTypeDetails> EcoPointTypeDetails { get; set; }
        public DbSet<EcoPointTypeHint> EcoPointTypeHints { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());
            modelBuilder.DefineRelationships();
        }
    }
}
