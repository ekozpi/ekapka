﻿using EkapkaBackend.Model.Entities;
using Microsoft.EntityFrameworkCore;

namespace EkapkaBackend.Model.ModelBuilderExtensions
{
    public static class DefineRelatonshipsExtensions
    {
        public static void DefineRelationships(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<EcoPoint>()
                .HasOne(p => p.Type)
                .WithMany(t => t.EcoPoints);

            modelBuilder.Entity<EcoPointTypeDetails>()
                .HasMany(d => d.Hints)
                .WithOne(h => h.Details);
        }
    }
}
