﻿using EkapkaBackend.Model.Entities;
using System.Collections.Generic;
using System.Linq;

namespace EkapkaBackend.Model.DataPumpers
{
    public static class AddressPumper
    {
        public static void Pump(EkapkaDbContext dbContext)
        {
            if (dbContext.Addresses.Any())
                return;

            dbContext.Addresses.AddRange(Addresses);
            dbContext.SaveChanges();
        }

        private static readonly List<Address> Addresses = new List<Address>()
        {
            new Address { Country = "Polska", State = "województwo dolnośląskie", City = "Wrocław", Street = "Prosta", Building = "Szkoła Podstawowa nr 97", Flat = "", Postcode = "53-509" },
            new Address { Country = "Polska", State = "województwo dolnośląskie", City = "Wrocław", Street = "Wita Stwosza", Building = "37", Flat = "", Postcode = "50-149" },
            new Address { Country = "Polska", State = "województwo dolnośląskie", City = "Wrocław", Street = "Szewska", Building = "8", Flat = "", Postcode = "50-122" },
            new Address { Country = "Polska", State = "województwo dolnośląskie", City = "Wrocław", Street = "Plac Dominikański", Building = "6", Flat = "", Postcode = "50-159" },
            new Address { Country = "Polska", State = "województwo dolnośląskie", City = "Wrocław", Street = "Nowa", Building = "Kompleks Sportowy IX LO i Liceum Plastycznego", Flat = "", Postcode = "50-124" },
            new Address { Country = "Polska", State = "województwo dolnośląskie", City = "Wrocław", Street = "Mennicza", Building = "Jednostka Ratowniczo-Gaśnicza PSP nr 1 we Wrocławiu", Flat = "", Postcode = "50-056" },
            new Address { Country = "Polska", State = "województwo dolnośląskie", City = "Wrocław", Street = "Podwale", Building = "73", Flat = "", Postcode = "50-449" }
        };
    }
}
