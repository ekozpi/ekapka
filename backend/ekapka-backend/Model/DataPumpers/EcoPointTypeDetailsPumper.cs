﻿using EkapkaBackend.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EkapkaBackend.Model.DataPumpers
{
    public static class EcoPointTypeDetailsPumper
    {
        public static void Pump(EkapkaDbContext dbContext)
        {
            if (dbContext.EcoPointTypeDetails.Any())
                return;

            List<Details> json = JsonConvert.DeserializeObject<List<Details>>(LoadJsonContent());

            foreach(Details details in json)
            {
                var type = dbContext.EcoPointTypes.Include(x => x.Details).FirstOrDefault(t => t.Slug == details.typeSlug);
                if (type == null)
                    return;

                var hints = new List<EcoPointTypeHint>();
                foreach (Hint hint in details.hints)
                {
                    hints.Add(new EcoPointTypeHint() { Title = hint.title, Content = hint.content, Url = hint.link });
                }

                var typeDetails = new EcoPointTypeDetails
                {
                    Description = details.description,
                    Hints = hints
                };

                type.Details = typeDetails;
            }

            dbContext.SaveChanges();
        }


        private static string LoadJsonContent()
        {
            string content;

            using (StreamReader sr = new StreamReader(@"Model\DataPumpers\Data\categories_details.json"))
            {
                content = sr.ReadToEnd();
            }

            return content;
        }

        private class Details
        {
            public string typeSlug;
            public string description;
            public List<Hint> hints;
        }

        private class Hint
        {
            public string title;
            public string content;
            public string link;
        }
    }
}
