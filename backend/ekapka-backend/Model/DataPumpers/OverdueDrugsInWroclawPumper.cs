﻿using EkapkaBackend.Model.Entities;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace EkapkaBackend.Model.DataPumpers
{
    public static class OverdueDrugsInWroclawPumper
    {
        public static void Pump(EkapkaDbContext dbContext)
        {
            if (dbContext.EcoPoints.Any(x => x.Name == "Apteka Drohobycka"))
                return;
      
            dbContext.EcoPoints.AddRange(ReadPoints(dbContext));
            dbContext.SaveChanges();
        }

        private static List<EcoPoint> ReadPoints(EkapkaDbContext dbContext)
        {
            var ecoPoints = new List<EcoPoint>();
            var overdueDrugsCategory = dbContext.EcoPointTypes.FirstOrDefault(x => x.Name == "Przeterminowane leki");

            using (var reader = new StreamReader(@"Model\DataPumpers\Data\nowe_przeterminowane_leki.txt"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var values = line.Split(';');
                    double.TryParse(values[0], NumberStyles.Any, CultureInfo.InvariantCulture, out double latitude);
                    double.TryParse(values[1], NumberStyles.Any, CultureInfo.InvariantCulture, out double longitude);
                    Address address = new Address { Country = values[3], State = values[4], City = values[5], Street = values[6], Building = values[7], Flat = values[8], Postcode = values[9] };
                    ecoPoints.Add(new EcoPoint() { Type = overdueDrugsCategory, Longitude = longitude, Latitude = latitude, Name = values[2], Date = DateTime.Now, Address = address });
                }
            }

            return ecoPoints;
        }
    }
}
