﻿using EkapkaBackend.Model.Entities;
using System.Collections.Generic;
using System.Linq;

namespace EkapkaBackend.Model.DataPumpers
{
    public static class EcoPointsPumper
    {
        public static void Pump(EkapkaDbContext dbContext)
        {
            if (dbContext.EcoPoints.Any())
                return;

            dbContext.EcoPoints.AddRange(EcoPoints(dbContext.EcoPointTypes.ToList(), dbContext.Addresses.ToList()));
            dbContext.SaveChanges();
        }

        private static List<EcoPoint> EcoPoints(List<EcoPointType> ecoPointTypes, List<Address> addresses) 
        {
            var ecoPoints = new List<EcoPoint>
            {
                new EcoPoint { Latitude = 51.104532, Longitude = 17.018975, Address = addresses[0], Name = "Lorem ipsum", Type = ecoPointTypes[0], Description = "Donec posuere quam porta ante commodo tristique. Fusce a malesuada velit. In accumsan elit quis euismod scelerisque. Mauris lacinia accumsan efficitur. Donec ornare tristique sodales. Vivamus vehicula metus ut metus consequat, venenatis semper nisi condimentum. Donec et rutrum arcu. Aenean pharetra nunc vitae justo convallis tristique. Nulla venenatis tortor quis venenatis commodo. Morbi ac diam at purus iaculis tincidunt non in nunc." },
                new EcoPoint { Latitude = 51.110093, Longitude = 17.036493, Address = addresses[1], Name = "Dolor sit amet", Type = ecoPointTypes[1], Description = "Mauris blandit erat at dolor vehicula, sit amet fermentum ligula molestie. Curabitur blandit sagittis nibh, vitae auctor enim porta ac. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Duis et iaculis elit. Vestibulum et leo vitae magna varius sollicitudin ac nec erat. In lacinia urna ac sapien egestas sagittis. Nulla tellus magna, lacinia in turpis non, molestie varius felis. Vestibulum ultricies felis sit amet sem semper, at ullamcorper ligula tempor. Morbi finibus luctus turpis congue ullamcorper. Nullam nec enim ornare, ultricies magna fringilla, placerat tellus." },
                new EcoPoint { Latitude = 51.108843, Longitude = 17.034238, Address = addresses[2], Name = "Lorem dolor", Type = ecoPointTypes[2], Description = "Pellentesque sit amet felis placerat, pulvinar enim nec, euismod est. Nunc molestie libero condimentum bibendum scelerisque. Quisque ut tempor ligula, nec aliquam nulla. Integer convallis urna fermentum diam cursus, eget tincidunt urna hendrerit. Proin sollicitudin sollicitudin orci eu tempus. Duis congue nunc eget arcu molestie, at egestas nibh euismod. Donec et turpis lacus. Proin bibendum, ex sit amet vulputate fringilla, lectus sapien malesuada nisi, eu posuere nulla sapien ac turpis. Curabitur semper leo nec pellentesque rutrum. Fusce tempor mollis aliquet." },
                new EcoPoint { Latitude = 51.109375, Longitude = 17.041313, Address = addresses[3], Name = "Homo sum", Type = ecoPointTypes[3], Description = "Curabitur eget est ut tortor aliquet aliquet vitae ut nulla. Donec nibh mi, ullamcorper et neque eget, cursus rhoncus nulla. Suspendisse sit amet nulla ut felis commodo cursus. Nulla magna augue, feugiat sed ullamcorper eu, pulvinar sit amet lorem. Etiam egestas, turpis quis ornare laoreet, est lacus tincidunt ex, a maximus lorem neque molestie eros. Proin nec vestibulum magna. Mauris vehicula, nisi ac lacinia elementum, libero dui ornare dolor, quis lobortis lectus odio at turpis. Aenean imperdiet sodales nisi, in faucibus nunc pulvinar vitae. Nulla sapien lacus, mattis eget est quis, sollicitudin molestie purus. Vivamus euismod scelerisque congue." },
                new EcoPoint { Latitude = 51.106275, Longitude = 17.039415, Address = addresses[4], Name = "Et nihil humanum", Type = ecoPointTypes[1], Description = "Curabitur vitae facilisis turpis. Nunc ultricies augue augue, quis mollis lacus tempus vel. Praesent et euismod nulla. In turpis turpis, dapibus quis ipsum ac, finibus tincidunt tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean elit neque, feugiat nec euismod sit amet, sagittis in odio. Vestibulum dapibus hendrerit augue, non iaculis magna accumsan ac. Nullam vel enim et massa condimentum consectetur at eget turpis. Vivamus tempus tristique mi, ut euismod nunc dapibus non. Integer tortor sapien, bibendum sit amet malesuada sed, hendrerit in elit." },
                new EcoPoint { Latitude = 51.105359, Longitude = 17.035649, Address = addresses[5], Name = "Ame alienum", Type = ecoPointTypes[2], Description = "Etiam urna odio, sollicitudin ac erat molestie, convallis faucibus urna. Integer tincidunt, libero luctus commodo viverra, turpis orci iaculis nibh, vel mollis sapien enim sed nibh. Nunc sit amet nunc condimentum odio finibus malesuada in vitae erat. Mauris ut lacus sed diam varius fringilla eget nec nibh. In dapibus odio dolor, vitae bibendum libero varius vitae. Etiam neque urna, congue non nibh sit amet, congue pellentesque nunc. In non sagittis dolor, elementum fermentum risus. Quisque volutpat felis ut leo elementum, gravida mollis libero vulputate." },
                new EcoPoint { Latitude = 51.105514, Longitude = 17.041785, Address = addresses[6], Name = "Esse puto", Type = ecoPointTypes[3], Description = "In euismod euismod metus, et dapibus urna eleifend vel. Sed sit amet urna et sem pretium mollis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer viverra dolor est, at condimentum lacus tempus et. Mauris finibus leo malesuada nulla ornare pretium. Suspendisse eget euismod libero. Suspendisse vel mattis risus, in vulputate ipsum. Aenean euismod purus lorem, in pellentesque turpis pellentesque a. Quisque vitae porta urna." }
            };

            return ecoPoints;
        }

    }
}
