﻿using EkapkaBackend.Model.Entities;
using System.Collections.Generic;
using System.Linq;

namespace EkapkaBackend.Model.DataPumpers
{
    public static class EcoPointTypesPumper
    {
        public static void Pump(EkapkaDbContext dbContext)
        {
            if (dbContext.EcoPointTypes.Any())
                return;

            var types = new List<EcoPointType>();

            for (int i = 0; i < CategoryNames.Count; i++)
                types.Add(new EcoPointType() { Name = CategoryNames[i], Slug = CategorySlugs[i] }); ;

            dbContext.EcoPointTypes.AddRange(types);
            dbContext.SaveChanges();
        }

        private static readonly List<string> CategoryNames = new List<string>()
        {
            "Zużyte baterie", "Niepotrzebne ubrania", "Przeterminowane leki", "Jadłodzielnia"
        };

        private static readonly List<string> CategorySlugs = new List<string>()
        {
            "spent-batteries", "unnecessary-clothes", "overdue-drugs", "foodsharing"
        };
    }
}
