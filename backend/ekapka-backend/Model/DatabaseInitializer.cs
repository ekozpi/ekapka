﻿using EkapkaBackend.Model.DataPumpers;

namespace EkapkaBackend.Model
{
    public static class DatabaseInitializer
    {
        public static void Initialize(EkapkaDbContext dbContext)
        {
            dbContext.Database.EnsureCreated();
           
            EcoPointTypesPumper.Pump(dbContext);
            AddressPumper.Pump(dbContext);
            EcoPointsPumper.Pump(dbContext);
            OverdueDrugsInWroclawPumper.Pump(dbContext);
            EcoPointTypeDetailsPumper.Pump(dbContext);
        }
    }
}
