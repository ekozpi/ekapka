﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EkapkaBackend.Model
{
    public class EmailSender
    {
        public static SendGridClient client = new SendGridClient(Environment.GetEnvironmentVariable("SENDGRID_API_KEY", EnvironmentVariableTarget.Machine));

        public static void SendEmail(string to_email, string to_name, string subject, string content_plain, string content_html)
        {
            var from = new EmailAddress("mateor78@gmail.com", "Ekapka Team");
            var to = new EmailAddress(to_email, to_name);
            var msg = MailHelper.CreateSingleEmail(from, to, subject, content_plain, content_html);
            client.SendEmailAsync(msg);
        }

        public static void SendConfirmationEmail(string to_email, string to_name, string confirmationLink)
        {
            var content_plain = $"Aby dokończyć rejestrację, zweryfikuj email przechodząc pod adres: {confirmationLink}";
            var content_html = $"<a href=\"{confirmationLink}\">Zweryfikuj email aby dokończyć rejestrację</a>";
            SendEmail(to_email, to_name, "Zweryfikuj swój email", content_plain, content_html);
        }
    }
}
