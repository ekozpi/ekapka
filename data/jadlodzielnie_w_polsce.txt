Bielsko-Biała - os. Karpackie;Jadłodzielnia znajduje się przy ul. Matusiaka 3, obok apteki.
Szafka i lodówka dostępne 24/7.;Klemensa Matusiaka 3, 43-316 Bielsko-Biała;
Bielsko Biała - Straconka;Jadłodzielnia znajduje się przy ulicy Górskiej 133, przy ośrodku zdrowia
Lodówka i szafka dostępne są 24/7;Górska 133, 43-318 Bielsko-Biała;
Bogatynia - Kościuszki;ul. Kościuszki 26,  pon-sob  7:00 - 18.00.;Kościuszki 26, 59-916 Bogatynia;
Bydgoszcz - WSG;ul. Królowej Jadwigi 14, otwarte codziennie 8-20 
strona na FB:
https://www.facebook.com/Jad%C5%82odzielnia-WSG-Karmnik-1449566785088417/;;
Czechowice-Dziedzice - Barabasza;Jadłodzielnia znajduje się przy ul. Barabasza 1, obok budynku Wydziału Spraw Obywatelskich Urzędu Miejskiego.
Szafka i lodówka dostępne są całą dobę.;Księdza Jana Nepomucena Barabasza 1, 43-502 Czechowice-Dziedzice;
Ełk - Małeckich;ul Małeckich 3, szafka i lodówka, dostępne 24/7

https://www.facebook.com/jadlodzielniaelk/;Jana i Hieronima Małeckich 3, 19-300 Ełk;
Gdańsk - 100cznia;tylko lodówka, wolnostojąca - na terenie przestrzeni kulturalno -gastronomicznej 100cznia. dostępna całą dobę
Jadłodzielnia działa sezonowo -  od maja do września.
ul. ks. Jerzego Popiełuszki 5;100cznia, Popiełuszki 5, 80-863 Gdańsk;
Grudziądz - Tarpno;Targowisko Miejskie Tarpno, ul. Paderewskiego, przy samym wejściu, otwarte codziennie 9-17;;
Jedlina-Zdrój - Piastowska;ul. Piastowska 12. Jadłodzielnia przylega do sklepu "Kupiec". Lodówka i szafka dostępne 24/7.;Piastowska 12, 58-330 Jedlina-Zdrój;
Jastrzębie-Zdrój - Wielkopolska;Jadłodzielnia w postaci budki stoi na małym parkingu za sklepem Stokrotka, szafka i lodówka, otwarte codziennie od 9:00 do 18:00. ;;
Kraków - Antykwariat PiBook;Jadłodzielnia dostępna od poniedziałku do soboty w godzinach 9-17.
Punkt znajduje się w drugim pomieszczeniu lokalu – po wejściu należy iść prosto do samego końca. Po prawej stronie zobaczycie lodówkę oraz regał.;Stefana Batorego 5, 33-332 Kraków;
Kraków - Spółdzielnia Ogniwo;ul. Paulińska 28, I piętro, lodówka i szafka, otwarte  codziennie w godzinach 16 - 22;Paulińska 28, 33-332 Kraków;
Kraków - Roślinny;Jadłodzielnia znajduje się w sklepie Roślinny (na drugiej sali, po lewej stronie) na Rynku Dębickim 10.
Dostępna pon-pt 10-19, sob 10-14.;Rynek Dębnicki 10, 30-305 Kraków;
Kraków - Skate Part Pool Forum;Jadłodzielnia znajduje się przy ul Marii Konopnickiej 28 (wejście od Ludwinowskiej 6).
Lodówka dostępna jest codziennie od 16 do 22;Marii Konopnickiej 28, 30-307 Kraków;
Krosno - Spółdzielcza;Jadłodzielnia znajduje się na ul Spółdzielczej 3.
Szafki i lodówka dostępne są pon-pt w godz. 7-17;Spółdzielcza, 38-400 Krosno;
Lublin - Targowisko nr 1;al. Tysiąclecia 2, Targowisko nr 1, pawilon 197, lodówka i szafka, czynne ;;
Łódź - EkSoc;Wydział Ekonomiczno-Socjologiczny Uniwersytetu Łódzkiego, ul. Polskiej Organizacji Wojskowej 3/5, szafka i lodówka, pn-pt 6-20, sob 7-20, ndz 8-18, "Na Wydział wchodzimy od strony ul. P.O.W. 3/5 i kierujemy się schodkami na dół do szatni budynku D.";Polskiej Organizacji Wojskowej 3, 90-001 Łódź;
Mosina - Zielony Rynek;Jadłodzielnia znajduje się przy Zielonym Rynku na ul. Farbiarskiej 17/19
Szafka i lodówka dostępne codziennie od 8 do 17.;Farbiarska 17/19, 62-050 Mosina;
Pruszków - Kraszewskiego;ul. Kraszewskiego 18, szafka i lodówka, otwarte codziennie 8-21;Józefa Ignacego Kraszewskiego 18, 05-803 Pruszków;
Rzeszów - Pub Kultura;Chopina 59,  Pub Kultura, otwarte w godzinach 10-22, tylko lodówka;Fryderyka Szopena 59, 35-055 Rzeszów;
Sosnowiec - Matejki;Jadłodzielnia znajduje się niedaleko sklepu spożywczego. Lodówka i szafka są w oznaczonej drewnianej altance.
Jadłodzielnia dostępna od pon-sob 6-22, ndz 8-20.;Jana Matejki 50, 41-219 Sosnowiec;
Słupsk - CIS;Ul.Jaracza 9
Przy Centrum Integracji Społecznej
lodówka i szafka
czynne: poniedziałek -piątek 7-15;Stefana Jaracza 9, 76-200 Słupsk;
Szczecin - Żółkiewskiego;ul. Żółkiewskiego 4 otwarte we wtorki i czwartki 11-17, w soboty 9:30 -12
strona na FB:
https://www.facebook.com/Jad%C5%82odzielnia-Szczecin-1831781530438716/
http://www.jadlodzielniaszczecin.pl/;;
Szczecin - Ryneczek Kilińskiego;Kilińskiego 1,  Ryneczek,  pawilon 65
otwarte: wtorek i czwartek 11:00-17:00 oraz sobota 9:30-12:00
http://www.jadlodzielniaszczecin.pl/;plac Kilińskiego 1, 71-414 Szczecin;
Szczecin - Stowarzyszenie Stołczyn Po Sąsiedzku;ul. Nad Odrą 18, szafka przy Stowarzyszeniu Stołczyn Po Sąsiedzku, 24/7
http://www.jadlodzielniaszczecin.pl/;Nad Odrą 18, 71-828 Szczecin;
Szczecin - C.H.U. Manhattan;ul. Księdza Stanisława Staszica 1, Centrum Jandlowe Manhattan, szafka, niedaleko sklepu Netto, 24/7
http://www.jadlodzielniaszczecin.pl/;Księdza Stanisława Staszica 1, 71-515 Szczecin;
Szczecin - Targowisko Zdroje;ul. Młodzieży Polskiej 26B, Targowisko Zdroje, szafka, czynna do godz. 18, później można przekazywać dary przez portiernię.
http://www.jadlodzielniaszczecin.pl/;Młodzieży Polskiej 26B, 70-774 Szczecin;
Szczecinek - Rzemieślnicza;ul. Rzemieślnicza 2/6, lodówka i szafka, otwarte pon-pt 11-12;Rzemieślnicza 2, 78-400 Szczecinek;
Szczytno - Chrobrego;ul. Bolesława Chrobrego 1, szafka i lodówka, wolnostojące, czynne 24/7, dodatkowo jest też wieszak do dzielenia się ubraniami.;;
Tarnów - BAZAR;Lodówka znajduje się w Centrum Handlowym  BAZAR i jest dostępna: pn-sob: 7-21, ndz: 9-17;Hodowlana 6, 33-100 Tarnów;
Toruń - Targowisko Manhattan;box nr 27, otwarte codziennie 6-22
strona na FB:
https://www.facebook.com/jadlodzielnia.torun;;
Toruń - Waryńskiego;Waryńskiego 19, otwarte 24/7
strona na FB:
https://www.facebook.com/jadlodzielnia.torun;;
Toruń - DS nr 3;ul. Stanisława Moniuszki 16/20, otwarte codziennie 7-20
strona na FB:
https://www.facebook.com/jadlodzielnia.torun;;
Tychy - Pasaż Andromeda;pl. Krzysztofa Kamila Baczyńskiego 2, lodówka i szafka, czynne codziennie 9-20;plac Krzysztofa Kamila Baczyńskiego 2, 43-100 Tychy;
Warszawa - Stawki, Wydział Psychologii;Stawki 5/7, Wydział Psychologii UW, szafka i lodówka, otwarte codziennie 8-21.
UWAGA! Czasem budynek może być zamknięty w tych godzinach - ale nie przejmujcie się - wtedy wystarczy zadzwonić dzwonkiem przy drzwiach (przy furtce) i pan stróż, który jest w budynku, wpuści was do Jadłodzielni :)

strona na FB:
https://www.facebook.com/FoodsharingWarszawa/;;
Warszawa - Jazdów, Motyka i Słońce;Jazdów 3/9, Domek Motyka i Słońce, tylko szafka, dostępna 24/7
uwaga Jadłodzielnia sezonowa, czynna tylko w ciepłej połowie roku!
strona na FB:
https://www.facebook.com/FoodsharingWarszawa/;;
Warszawa - Stół Powszechny;Jadłodzielnia znajduje się w kawiarni Stół Powszechny w budynku Teatru Powszechnego. Szafka na zewnątrz, dostępna 24/7, lodówka w środku, otwarta codziennie 11-22
ul. Jana Zamoyskiego 20

strona na FB:
https://www.facebook.com/FoodsharingWarszawa/;;
Warszawa - Karowa;Karowa 18, Instytut Socjologii UW, szafka i lodówka, otwarte codziennie 8-21
UWAGA! Czasem budynek może być zamknięty w tych godzinach - ale nie przejmujcie się - wtedy wystarczy zadzwonić dzwonkiem przy drzwiach (w futrynie) i pan stróż, który jest w budynku, wpuści was do Jadłodzielni :)
strona na FB:
https://www.facebook.com/FoodsharingWarszawa/;;
Warszawa - Bazar Lotników;Bazar Lotników, Pawilon 83 A, otwarte pon-pt 7-19, sob 8-14
Al. Lotników 1
strona na FB:
https://www.facebook.com/FoodsharingWarszawa/;;
Warszawa - SGGW;Wydział Technologii Drewna, Nowoursynowska 159, budynek 34, klatka B, lodówka i szafka, otwarte codziennie 8-22;Nowoursynowska 159, 02-776 Warszawa;
Warszawa - Paca;ul. Michała Paca 40, Centrum Społeczne Paca
szafka i lodówka, otwarte pon-pt 10-20, sb 10-18;;
Warszawa - CPS Śródmieście;ul. Twarda 1, Jadłodzielnia mieści się w placówce Centrum Pomocy Społecznej dzielnicy Śródmieście.
lodówka i szafka, czynne pon-pt 8-16 (ew do 18);Twarda 1, 00-824 Warszawa;
Warszawa - MAL Dwa Jelonki;ul. Powstańców Śląskich 44, lodówka i szafka;Powstańców Śląskich 44, 01-381 Warszawa;
Warszawa - Urząd Dzielnicy Żoliborz;ul. Słowackiego 6/8, Jadłodzielnia to wolnostojąca szafka z lodówką, na tyłach budynku Urzędu Dzielnicy Żoliborz.
czynna 24/7;;
Warszawa - KEN;Jadłodzielnia znajduje się przy Ratuszu Ursynów na Komisji Edukacji Narodowej 61. Lodówka i szafka są dostępne 24/7.;Aleja Komisji Edukacji Narodowej 61, 02-777 Warszawa;
Warszawa - OPS Praga-Południe;Jadłodzielnia znajduje się w Ośrodku Pomocy Społecznej na Wiatracznej 11. Dostępne szafka i lodówka. Z Jadłodzielni korzystać można w godzinach otwarcia OPS: pon.-pt. 8-16;Wiatraczna 11, 04-366 Warszawa;
Warszawa - WCK Działdowska;Jadłodzielnia przy ul. Działdowskiej 6, od strony podwórka.
Szafka i lodówka dostępne: pon-pt 8-21,  sob-ndz 12-20.;Działdowska 6, 01-184 Warszawa;
Warszawa - OPS ”Chatka Puchatka”;Jadłodzielnia znajduje się przy budynku OPS ”Chatka Puchatka” na ul. Węgrowskiej 2.
Szafka dostępna 24h na dobę;Węgrowska 3, 03-507 Warszawa;
Warszawa - Wincentego;ul. Wincentego 85. Jadłodzielnia znajduję się przy wejściu do Biblioteki oraz Środowiskowego Domu Samopomocy.
Szafka dostępna pon-pt 8:00-19:00;św. Wincentego 85, 03-341 Warszawa;
Warszawa - Zieleniak;Jadłodzielnia mieści się przy ul. Grójeckiej 97. Szafka i lodówka dostępne 24/7.;Grójecka 97, 02-120 Warszawa;
Warszawa - Zakopiańska;Jadłodzielnia znajduje się w ogródku na ul. Zakopiańskiej 21.
Szafka dostępna 24h na dobę.;Zakopiańska 21, 03-934 Warszawa;
Warszawa - SOdON;Jadłodzielnia mieści się w Stołecznym Ośrodku dla Osób Nietrzeźwych. Po minięciu drzwi znajduje się po lewej stronie. Szafka i dwie małe lodówki dostępne są codziennie od godz 5 do 22.;Kolska 2/4, 01-045 Warszawa;
Warszawa - Stojanowska;Jadlodzielnia znajduje się w Filii nr 2 OPS Targówek, przy ul. Stojanowskiej 12/14.

Szafka dostępna pon-pt 8-16;Stojanowska 12/14, 03-558 Warszawa;
Wrocław - HART Hostel & Art;ul. Ludwika Rydygiera 25a, 50-248 Wrocław
Dziedziniec artystyczny, przy HART - Hostel & Art
czynne całą dobę
https://www.facebook.com/Wspolnalodowkananadodrzu/
https://www.facebook.com/foodsharingwroclaw/;Ludwika Rydygiera 25a, 50-248 Wrocław;
Wrocław - Grabiszyn;Jadłodzielnia znajduje się przy ul. Gajowickiej 96a.
Lodówka jest przed siedzibą Rady Osiedla Powstańców Śląskich, niedaleko kościoła przy ul. Kruczej. Jest widoczna z ulicy.
Dostępna całą dobę.;Gajowicka 96a, 53-422 Wrocław;
Wrocław - Swojczyce;Jadłodzielnia znajduje się przy ul. Swojczyckiej 118.
Szafka i lodówka znajdują się z przodu budynku, między wejściem do Rewiru, a wejściem do Rady Osiedla
Dostępne całą dobę.;Swojczycka 118, 51-502 Wrocław;
Wrocław - Hercena;Jadłodzielnia znajduje się przy ul. Hercena 13
Lodówka dostępna całą dobę.;Aleksandra Hercena 13, 50-453 Wrocław;
Wrocław - Ołbin;Jadłodzielnia znajduje się na rogu ulic Prusa i Wyszyńskiego, przy Rewizja grupa projektowa.

Lodówka dostępna jest całą dobę.;Prusa 37, 50-319 Wrocław;
Wrocław - Muchobór;Jadłodzielnia zlokalizowana jest na rogu ulic Mińskiej i Zagony, przy Zgromadzeniu Sióstr Służebniczek (ul. Zagony 63).

Lodówka dostępna jest całą dobę.;Zagony 63, 54-614 Wrocław;
Zamość - Spichlerz Brata Alberta;ul. Dembowskiego 24, przy kościele św. Brata Alberta, szafki oraz lodówka, otwarte codziennie 7-19;Edwarda Dembowskiego 24, 22-400 Zamość;
Zielona Góra - Polski Komitet Pomocy Społecznej;ul. Moniuszki 35. Jadłodzielnia całodobowa, wolnostojąca, z szafką i lodówką.;;
Konstancin-Jeziorna - OSP Skolimów;ul. Pułaskiego 72, szafka i lodówka, punkt przy Ochotniczej Straży Pożarnej Skolimów,
Szafka i lodówka dostępne codziennie 24h/dobę.;Pułaskiego 72, 05-510 Konstancin-Jeziorna;
Kraków - Tytano;ul. Dolnych Młynów 10,  przy stróżówce, przy wjeździe/wyjeździe na teren Tytano., koło szlabanu. Lodówka i szafka, czynne: ?????;Dolnych Młynów 10, 33-332 Kraków;
Leszno - Lodówka dla Głodnych;ul. Towarowa 10, Noclegownia dla Bezdomnych Mężczyzn. ;Towarowa 10, 64-100 Leszno;
Milanówek - Lodówka przy Królewskiej;Tylko lodówka, na terenie firmy Pikomp w Milanówku przy ul. Królewskiej 61A vis a vis stacji kolejki WKD Milanówek-Grudów. Czynna ;Królewska 61A, 05-822 Milanówek;
Oborniki - Lodówka fundacji Weź Pomóż;Punkt przy kościele Rzymskokatolickim Pw. Miłosierdzia Bożego w Obornikach

Droga Leśna 60, 64-600 Oborniki;Droga Leśna 60, 64-600 Oborniki;
Piła - Współdzielnia na Targowisku;Targowisko Miejskie, ul. Ludowa, 11b, lodówki i szafki, miejsce działające niezależnie od Foodsharing Polska, zarówno do dzielenia się jedzeniem, jak i innymi produktami. czynne pon-sob 9-15;Ludowa 11B, 64-920 Piła;
Poznań - Jadłodzielnia Wildecka;Rynek wildecki, stragan 78, tylko skrzynki, czynne 24/7;61-568 Poznań, Polska;
Sierpce - Lodówka Sierpecka;ul. Piastowska 42, pomiędzy sklepem Foral a apteką, lodówka, czynna 24/7;Piastowska 42, 09-200 Sierpc;
Sosnowiec - Punkt w SCOP;Sosnowieckie Centrum Organizacji Pozarządowych, pl. Kościuszki 5;plac Kościuszki 5, 41-205 Sosnowiec;
Sosnowiec - Punkt w Urzędzie Miasta;ul. .3 maja 33, Urząd Miasta, parter, tylko szafka;3 Maja 33, 41-200 Sosnowiec;
Sosnowiec - Punkt w OIK;Ośrodek Interwencji Kryzysowej, ul. Karola Szymanowskiego 5a, szafka i lodówka;Karola Szymanowskiego 5A, 41-219 Sosnowiec;
Warszawa - Jadłodzielnia Syrena;Wilcza 30, Jadłodzielnia czynna 24/7. Szafka  jest widoczna z ulicy, mieści się w witrynie zewnętrznej strony kamienicy, lodówka za bramą, również dostępna całą dobę.
UWAGA! w sezonie zimowym  dostępna jest tylko szafka, lodówka na zimę jest chowana.;;
Warszawa - Klaudyny;Lodówka znajduję się przy na Klaudyny 38 przy placu zabaw. Nie jest podłączona do prądu.
Działa 24/7;Klaudyny 38, Warszawa;
Warszawa - Smocza;Jadłodzielnia znajduje się w wejściu do Cafe PoWoli przy ul. Smoczej 3.
Lodówka dostępna od poniedziałku do piątku w godzinach 9:00-18:00 i w soboty 10:00-16:00.;Smocza 3, 00-151 Warszawa;
Wrocław - Szczepin - Zachodnia;Lodówka - ul. Zachodnia 1 (Szczepin)
opis
Społeczna lodówka powstała z inicjatywy organizacji Weźpomóż.pl (http://wezpomoz.pl/).
Punkt czynny całą dobę.

Jak trafić?
Lodówka przy Radzie Osiedla Szczepin;Zachodnia 1, Wrocław;
Wrocław - Osobowice;Lodówka - ul. Gajowicka 96a (Grabiszyn)
opis
Osiedlowa Lodówka Społeczna powstała z inicjatywy Rady Osiedla Powstańców Śląskich.
Punkt czynny całą dobę.

Jak trafić?
Lodówka przed siedzibą Rady Osiedla Powstańców Śląskich, niedaleko kościoła przy ul. Kruczej. Jest widoczna z ulicy.
https://www.facebook.com/wezpomoz/videos/215514359026550/;Osobowicka 129, 51-004 Wrocław;
Wrocław - Zakrzów;Lodówka - ul. Okulickiego 2 (Zakrzów)
opis
Społeczna lodówka powstała z inicjatywy stowarzyszenia Zmieniaj Zakrzów (https://www.facebook.com/ZmieniajZakrzow/), Fundacji Przyjazny Dom im. Stanisława Jabłonki, Mieszkańców Zakrzowa oraz przy wsparciu Fundacji Weź Pomóż.

Jak trafić?
Lodówka znajduje się przy Fundacji Przyjazny Dom im. Stanisława Jabłonki
Punkt czynny całą dobę.;Okulickiego 2, 51-216 Wrocław;
Wrocław - Stare Miasto;Lodówka - ul. Kazimierza Wielkiego 31-33 (Stare Miasto)
opis
Społeczna lodówka powstała z inicjatywy Kamienicy pod Aniołami, Parafii Ewangelicko-Augsburskej Opatrzności Bożej, Rady Osiedla Stare Miasto oraz przy wsparciu Fundacji Weź Pomóż.

Jak trafić?
Lodówka znajduje się przy Kamienicy pod Aniołami.
Punkt czynny całą dobę.;;
Wrocław - Przedmieście Świdnickie;Lodówka - ul. Grabiszyńska 56 (Przedmieście Świdnickie)
opis
Społeczna lodówka powstała z inicjatywy Kawiarni Sąsiedzkiej, Firlej - Ośrodek Działań Artystycznych, Rady Osiedla Przedmieście Świdnicki oraz przy wsparciu Fundacji Weź Pomóż.

Jak trafić?
Lodówka znajduje się przy Kawiarni Sąsiiedzkiej.
Punkt czynny całą dobę.;Grabiszyńska 56, 53-504 Wrocław;
Wrocław - Ołbin;Lodówka - ul. Odona Bujwida 51 (Ołbin)
opis
Społeczna lodówka powstała z inicjatywy Parafii św. Wawrzyńca i Rady Osiedla Ołbin oraz przy wsparciu Fundacji Weź Pomóż.

Jak trafić?
Lodówka znajduje się przy kościele
Punkt czynny w godz. 8-19;Bujwida 51, 50-368 Wrocław;
Wrocław - Szczepin;Lodówka - ul. Strzegomska 49 (Szczepin)
opis
Społeczna lodówka powstała z inicjatywy Wrocławskiego Centrum Integracji, Towarzystwa Pomocy im. św. Brata Alberta ― Koło Wrocławskie! oraz przy wsparciu Fundacji Weź Pomóż.

Jak trafić?
Lodówka znajduje się tuż przy Wrocławskim Centrum Integracji. Należy przejść przez metalową bramkę i kierować się prosto do głównego wejścia. Lodówka znajduje się tuż za małym, szarym budynkiem po lewej stronie.

Punkt czynny całą dobę;Strzegomska 49, 53-611 Wrocław;
Wrocław - Huby;Lodówka - ul. Gliniana 16 (Huby)
opis
Społeczna lodówka powstała z inicjatywy Parafii Św, Henryka we Wrocławiu oraz przy wsparciu Fundacji Weź Pomóż.

Jak trafić?
Lodówkę znajduje się przy Kościele św. Henryka, od strony ulicy Glinianej.;Gliniana 16, 50-525 Wrocław;
Wrocław - Krupnicza;Lodówka - ul. Krupnicza 15 (Stare Miasto)
opis
Społeczna lodówka powstała z inicjatywy Klubu Sportowego Gwardia Wrocław oraz przy wsparciu Fundacji Weź Pomóż.

Jak trafić?
Punkt mieści się niedaleko rynku, naprzeciwko NFM, przy przystanku NFM. Lodówka stoi tuż przy wejściu do KS Gwardia Wrocław. Należy tylko wspiąć się na kilka schodków i przejść przez metalową bramę. Lodówka widoczna jest z ulicy. ;Krupnicza 15, 50-043 Wrocław;
Wrocław - Grabiszynek;Lodówka znajduje się przy ul. Grabiszyńskiej 184
Społeczna lodówka powstała z inicjatywy Pana Krzysztofa Molicza przy wsparciu organizacji Weźpomóż.pl (http://wezpomoz.pl/).
Punkt czynny całą dobę.

Jak trafić?
Lodówka znajduje się koło budynku administracji Centrum Historii Zajezdnia (od strony ul. Inżynierskiej).;Grabiszyńska 184, 53-235 Wrocław;
Wrocław - Namysłowska;Lodówka znajduje się przy ul. Namysłowska 8

Społeczna lodówka powstała dzięki współpracy z CB Grafit oraz Fundacji weźpomóż.pl (http://wezpomoz.pl/).

Jak trafić?
Lodówka znajduję w przedsionku wejścia do Centrum Biznesowego Grafit.

Lodówka czynna w godzinach otwarcia CB Grafit.;Namysłowska 8, 50-302 Wrocław;
Wrocław - Swojczyce;Lodówka znajduje się przy ul. Swojczyckiej 82
Lodówka powstała z inicjatywy właścicielki Sklepu Eko Tytka oraz przy wsparciu Weźpomóż.

Jak trafić?
Lodówka znajduje się na tuż przy wejściu do sklepu Eko Tytka/

Punkt czynny całą dobę.;Swojczycka 82, 51-503 Wrocław;
