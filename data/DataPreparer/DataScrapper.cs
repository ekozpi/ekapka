﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using ExpectedConditions = SeleniumExtras.WaitHelpers.ExpectedConditions;

namespace DataPreparer
{
    class DataScrapper
    {
        public DataScrapper(string url)
        {
            Url = url;
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(2));
            longWait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
        }

        protected string Url { get; set; }
        protected List<MarkerData> ScrappedData { get; set; } = new List<MarkerData>();

        protected readonly ChromeDriver driver;
        protected readonly WebDriverWait wait;
        protected readonly WebDriverWait longWait;

        public List<MarkerData> ScrapeData()
        {

            try
            {
                driver.Navigate().GoToUrl(Url);

                RevealAllPoints();
                List<IWebElement> points = GetAllPoints();

                foreach (var point in points)
                {
                    if (!point.Displayed || !point.Enabled)
                    {
                        Thread.Sleep(200);
                    }

                    ScrappedData.Add(TransformPointToMarkerData(point));
                }
            } finally
            {
                driver.Dispose();
            }

            return ScrappedData;
        }


        private void RevealAllPoints()
        {
            var revealeres = FindElements(By.XPath("//span[@class = 'HzV7m-pbTTYe-bN97Pc-ti6hGc-z5C9Gb']"));
            foreach (var revealer in revealeres)
            {
                revealer.Click();
            }
        }

        private List<IWebElement> GetAllPoints()
        {
            return FindElements(By.XPath("//div[@class='HzV7m-pbTTYe-ibnC6b pbTTYe-ibnC6b-d6wfac HzV7m-pbTTYe-ibnC6b-onv9We']"));
        }

        private MarkerData TransformPointToMarkerData(IWebElement point)
        {
            var markerData = new MarkerData();
            point.Click();

            markerData.Name = GetMarkerCommonValue("nazwa");
            markerData.Description = GetMarkerCommonValue("opis");
            markerData.Address = GetMarkerAddress();
            //SetMarkerCoords(markerData);
            SafeClick(By.XPath("//span[@class='HzV7m-tJHJj-LgbsSe-Bz112c qqvbed-a4fUwd-LgbsSe-Bz112c']"));

            return markerData;
        }

        private string GetMarkerCommonValue(string label)
        {
            try
            {
                var elem = FindElement(By.XPath($"//div[@class='qqvbed-p83tee-V1ur5d' and text()='{label}']//following::div[@class='qqvbed-p83tee-lTBxed'][1]"));
                return elem.Text;
            } catch (Exception)
            {
                Console.WriteLine("Nie udało się wczytać wartości dla labela: " + label); 
            }

            return string.Empty;
        }

        private string GetMarkerAddress()
        {
            try
            {
                var elem = FindElement(By.XPath("//div[@class='fO2voc-jRmmHf-MZArnb-Q7Zjwb']"));
                return elem.Text;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return string.Empty;
        }

        private void SetMarkerCoords(MarkerData markerData)
        {

            var latLongConverterUrl = "https://www.latlong.net/convert-address-to-lat-long.html";

            driver.ExecuteScript($"window.open('{latLongConverterUrl}','_blank');");
            var tabs = new List<string>(driver.WindowHandles);

            driver.SwitchTo().Window(tabs[1]);

            FindElement(By.TagName("input")).SendKeys(markerData.Address);
            SafeClick(By.XPath("//button[text()='Find']"));

            markerData.Latitude = FindElement(By.Id("lat")).Text;
            markerData.Longitude = FindElement(By.Id("lng")).Text;
            driver.ExecuteScript("window.close();");

            driver.SwitchTo().Window(tabs[0]);
        }

        private void SafeClick(By by)
        {
            wait.Until(ExpectedConditions.ElementToBeClickable(by));
            driver.FindElement(by).Click();
        }


        private IWebElement FindElement(By by)
        {
            wait.Until(ExpectedConditions.ElementIsVisible(by));
            return driver.FindElement(by);
        }

        private List<IWebElement> FindElements(By by)
        {
            wait.Until(ExpectedConditions.PresenceOfAllElementsLocatedBy(by));
            return driver.FindElements(by).ToList();
        }
    }
}
