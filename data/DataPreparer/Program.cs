﻿using System;
using System.Collections.Generic;

namespace DataPreparer
{
    class Program
    {
        static void Main(string[] args)
        {
            var actions = new List<Tuple<string, string>>()
            {
                Tuple.Create(
                    "https://www.google.com/maps/d/viewer?mid=1vpCSdHuflmBIw4WWV3VFCQ4L2sU&ll=49.80954500000002%2C19.063600899999983&z=12&fbclid=IwAR3pYY2EBPe5wDVk8QJ3LAVnRfzNN4Lw3305SAjr4rj0L7yoqPIPGpnSvrA",
                    "jadlodzielnie-w-polsce.txt"
                ),
            };


            foreach(var action in actions)
            {
                var data = new DataScrapper(
                    action.Item1
                ).ScrapeData();

                DataSaver.Save(action.Item2, data);
            }
        }
    }
}
