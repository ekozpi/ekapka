﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataPreparer
{
    class MarkerData
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string Address { get; set; }

        public string Longitude { get; set; }

        public string Latitude { get; set; }

        public List<string> PhotoUrls { get; set; }

        public override string ToString()
        {
            return Name + ";" + Description + ";" + Address + ";";
        }

        private string SplitAddress()
        {
            if (Address == null || Address == string.Empty)
                return Address;

            var splitAddress = Address.Split(" ");
            
            return splitAddress[0] + ";" + splitAddress[1].Substring(0, splitAddress[1].Length - 1) 
                + ";" + splitAddress[2] + ";" + splitAddress[3] + ";";
        }
    }
}
