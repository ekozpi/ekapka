﻿using System;
using System.Collections.Generic;
using System.IO;

namespace DataPreparer
{
    class DataSaver
    {     
        public static void Save(string fileName, List<MarkerData> markersData)
        {
            string workingDirectory = Environment.CurrentDirectory;
            string projectDirectory = Directory.GetParent(workingDirectory).Parent.Parent.Parent.FullName;

            using (StreamWriter outputFile = new StreamWriter(Path.Combine(projectDirectory, fileName)))
            {
                foreach (var marker in markersData)
                    outputFile.WriteLine(marker);
            }
        }
    }
}
