# README #

### What software do you need to run this app? ###

* Net Core 3.1
* Visual Studio 2017/2019 (backend)
* Visual Studio Code (frontend)
* Node.js + npm
* SQL Server + SQL Management Studio

### How to run this app? ###

Backend:

* Have SQL Server configured
* Open backend.sln as admin
* Edit connectionStrings to your own in appsettings.json if needed (depends on SQL Server config)
* Open packet maneger console window
* Type Update-Database to create database connected with project
* Run project

Frontend:

* Open folder ekapka_front in Visual Studio Code
* Open built-in console
* Type npm install to download node_modules
* Type npm start to run project

